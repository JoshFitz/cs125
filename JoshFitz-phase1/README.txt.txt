Josh Fitzgerald

Acknowledgements: Professor Rodkey, Ryley Oroku, Landon Moir, and the members of my group

Bitbucket Repository URL: https://bitbucket.org/JoshFitz/cs125/src/master/

Files contained in this zip folder: 
	- README.txt
		- This file contains a "table of contents" for the contents of the submission, giving credit to any help received as well
	- BidgoodOverview.pdf
		- This pdf contains a picture of the E/R Diagram used, a description of the assignment, conversion of E/R diagram to relations and FD's, and an analysis of decisions made when creating the E/R Diagram
	- ERD.png
		- This is the png file that was included in the previous pdf file, but by itself