from os import write
from types import BuiltinMethodType
from faker import Faker
import random

sqlFile = open("populatelarge.sql", "w")
sqlFile.flush()
faker = Faker()


manufacturers = []
businesses = ["corp", "technologies", "networks", "computing", "cloud services", "engineering", "solutions"]
for i in range(100):
    businessName = faker.last_name() + "-" + faker.word(businesses)
    manufacturers.append((
        #"\'MAN" + "{0:03d}".format(i) + "\'", #--use for VARCHAR manf_id
        str(i),
        "\'" + businessName + "\'",
        "\'www." + businessName + ".com/support\'"))

for manufacturer in manufacturers: 
    sqlFile.write(
        "INSERT INTO manufacturer VALUES (" +
        manufacturer[0] + ", " +
        manufacturer[1] + ", " +
        manufacturer[2] + ");\n")


buildings = []
buildingTypes = ["Hall", "Center", "Theatre", "Offices", "Field", "Library", "Gymnasium"]

def genBuilding(count):
    buildingName = faker.last_name() + " " + faker.word(buildingTypes)
    return (
        #"\'" + buildingName.split(" ")[0][:4] + buildingName.split(" ")[1][0] + "\'", #--to generate a VARCHAR bldg_id
        str(count), #--to generate an INT bldg_id
        "\'" + buildingName + "\'")

for i in range(100):
    building = genBuilding(i)
    while building in buildings:
        building = genBuilding(i)
    buildings.append(building)

for building in buildings:
    sqlFile.write(
        "INSERT INTO building VALUES (" +
        building[0] + ", " +
        "\'main\', " +
        building[1] + ", " +
        str(faker.random_int(1, 20)*100) + ");\n"
    )


rooms = []
descriptions = ["General-purpose classroom", "Theatre", "Cleaning closet", "Lecture hall", "Broom cupboard", "Server room", "Break room", "Water closet"]

def genRoom(buildingID, number):
    room_id = int(buildingID)*100 + int(number) #--use when generating INT room_id
    return (
        #"" + buildingID[:-1] + "-1{0:02d}".format(number) + "\'", #--to generate a VARCHAR room_id
        str(room_id), #--to generate an INT room_id     !IMPORTANT! both VARCHAR and INT versions of buildings and rooms must be used
        "\'" + faker.word(descriptions) + "\'",
        buildingID 
    )

for building in buildings:
    for i in range(1,11):
        rooms.append(genRoom(building[0], i))

for room in rooms:
    sqlFile.write(
        "INSERT INTO room VALUES (" +
        room[0] + ", " +
        room[1] + ", " +
        room[2] + ");\n"
    )

racks = []
rackRoomID = ""
#slotIDs = ["\'A\'", "\'B\'", "\'C\'", "\'D\'", "\'E\'", "\'F\'", "\'G\'", "\'H\'", "\'I\'", "\'J\'"] #--use for VARCHAR slot id
slotIDs = [0, 1 , 2, 3, 4, 5, 6, 7, 8, 9] #--use for INT slot id

for room in rooms:
    if room[1] == "\'Server room\'":
        rackRoomID = room[0]
        break

for i in range(10):
    for id in slotIDs:
        racks.append((
            id,
            #"\'R{0:03d}".format(i) + "\'", #--use for VARCHAR rack id
            rackRoomID + str(id*100 + i), #--use for INT rack id
            rackRoomID
        ))

for rack in racks:
    sqlFile.write(
        "INSERT INTO rack VALUES (" +
        str(rack[0]) + ", " +
        str(rack[1]) + ", " +
        str(rack[2]) + ");\n"
    )


server_models = []

alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

for i in range(100):
    server_models.append((
        "\'SVR-" + random.choice(alphabet) + random.choice(alphabet) + random.choice(alphabet) + "{0:04d}\'".format(i),
        "\'OUTTEL" + str(random.randrange(3,9)) + "\'",
        "\'" + str(random.randrange(4,33)) + "gb\'",
        "\'plinux" + str(random.randrange(2,5)) + "\'",
        "\'" + str(random.randrange(1,5)) + "tb\'",
        #"\'whatever-this-is\'", #--use for VARCHAR virtual attribute
        str(random.choice(["true", "false"])), #--use for BOOLEAN virtual attribute
        random.choice(manufacturers)[0]
    ))


camera_models = []

for i in range(100):
    camera_models.append((
        "\'CAM-" + random.choice(alphabet) + random.choice(alphabet) + random.choice(alphabet) + "{0:04d}\'".format(i),
        #random.choice(["\'photo\'", "\'video\'", "\'both\'"]), #--use for VARCHAR recording attribute
        random.choice(["true", "false"]), #--use for BOOLEAN recording attribute
        #"\'" + str(random.randrange(3, 10)*10) + "ft\'", #--use for VARCHAR range attribute
        str(random.randrange(20, 1000)), #--use for INT range attribute
        "\'1080p\'", 
        #"\'" + str(random.randrange(5, 10)*10) + "deg\'", #--use for VARCHAR field_of_view attribute
        str(random.randrange(5, 10)*10),
        random.choice(manufacturers)[0]
    ))


projector_models = []

for i in range(100):
    projector_models.append((
        "\'PRO-" + random.choice(alphabet) + random.choice(alphabet) + random.choice(alphabet) + "{0:04d}\'".format(i),
        #random.choice(["\'yes\'", "\'no\'"]), #--use for VARCHAR hdmi attribute
        random.randrange(1, 10), #--use for INT hdmi attribute
        random.choice(["true", "false"]),
        #"\'" + str(random.randrange(10, 90) * 10) + "lu\'", #--use for VARCHAR lumens attribute
        str(random.randrange(10, 90)), #--use for INT lumens attribute
        random.choice(["true", "false"]),
        "\'1080p\'", 
        random.choice(manufacturers)[0]
    ))


printer_models = []

for i in range(100):
    printer_models.append((
        "\'PRI-" + random.choice(alphabet) + random.choice(alphabet) + random.choice(alphabet) + "{0:04d}\'".format(i),
        random.choice(["true", "false"]),
        "\'" + str(random.randrange(10, 100)*10) + "\'",
        #"\'" + str(random.randrange(8,33)) + "gb\'", #--use for VARCHAR memory attribute
        str(random.randrange(3, 33)), #--use for INT memory attribute
        #"\'" + str(random.randrange(1,4)*10) + "ppm\'", #--use for VARCHAR speed attribute
        str(random.randrange(1, 4)*10), #--use for INT speed attribute
        random.choice(["true", "false"]),
        #random.choice(["true", "false"]), #--use for BOOLEAN bw_color attribute
        random.choice(["bw", "color", "both"]), ##--use for VARCHAR bw_color attribute
        random.choice(manufacturers)[0]
    ))


computer_models = []

for i in range(100):
    computer_models.append((
        "\'COM-" + random.choice(alphabet) + random.choice(alphabet) + random.choice(alphabet) + "{0:04d}\'".format(i),
        "\'OUTTEL" + str(random.randrange(3,9)) + "\'",
        #"\'" + str(random.randrange(11,18)) + "in\'", #--use for VARCHAR screen_size attribute
        str(random.randrange(11,18)), #--use for INT screen_size attribute
        "\'1080p\'",
        #random.choice(["\'8", "\'16", "\'32", "\'64"]) + "gb\'", #--use for VARCHAR ram attribute
        random.choice(["8", "16", "32", "64"]), #--use for INT ram attribute
        "\'linux " + str(random.randrange(2,5)) + "\'",
        random.choice(["\'laptop\'", "\'desktop\'"]),
        #random.choice(["\'128", "\'256", "\'512", "\'1024"]) + "gb\'", #--use for VARCHAR hd_size attribute
        random.choice(["128", "256", "512", "1024"]), #--use for INT hd_size attribute
        random.choice(manufacturers)[0]
    ))


monitor_models = []

for i in range(100):
    monitor_models.append((
        "\'MON-" + random.choice(alphabet) + random.choice(alphabet) + random.choice(alphabet) + "{0:04d}\'".format(i),
        #"\'" + str(random.randrange(1,4)) + "\'", #--use for VARCHAR hdmi attribute
        str(random.randrange(1,4)), #--use for INT hdmi attribute
        "\'whatever\'",
        "\'1080p\'",      
        #"\'" + str(random.randrange(14,27)) + "in\'", #--use for VARCHAR size attribute
        str(random.randrange(14,27)), #--use for INT size attribute
        #"\'" + str(random.randrange(3,7)*10) + "ghtz\'", #--use for VARCHAR refresh_rate attribute
        str(random.randrange(3,7)*10), #--use for INT refresh_rate attribute
        random.choice(manufacturers)[0]
    ))


nwdevice_models = []

for i in range(100):
    nwdevice_models.append((
        "\'NWD-" + random.choice(alphabet) + random.choice(alphabet) + random.choice(alphabet) + "{0:04d}\'".format(i),
        "\'random-addr\'",
        "\'some-protocol\'",
        random.choice(["\'access-point\'", "\'server\'", "\'booster\'"]),   
        #"\'" + str(random.randrange(3,7)) + "gb/s\'", #--use for VARCHAR speed attribute  
        str(random.randrange(3,7)), #--use for INT speed attribute
        str(random.randrange(3,6)),
        #"\'" + str(random.randrange(3,10)*10) + "yrds\'", #--use for VARCHAR coverage attribute
        str(random.randrange(3,10)*10), #--use for INT coverage attribute
        str(random.randrange(30,100)*10),
        random.choice(manufacturers)[0]
    ))


departments = [
    ("\'ART\'", "\'Art\'", 0, "\'major studies\'"),
    ("\'BIO\'", "\'Biology\'", 0, "\'major studies\'"),
    ("\'CHE\'", "\'Chemistry\'", 0, "\'major studies\'"),
    ("\'COM\'", "\'Communication Studies\'", 0, "\'major studies\'"),
    ("\'CMP\'", "\'Computer Science\'", 0, "\'major studies\'"),
    ("\'ECO\'", "\'Economics and Business\'", 0, "\'major studies\'"),
    ("\'EDU\'", "\'Education\'", 0, "\'major studies\'"),
    ("\'EGI\'", "\'Engineering\'", 0, "\'major studies\'"),
    ("\'ENG\'", "\'English\'", 0, "\'major studies\'"),
    ("\'FRE\'", "\'French\'", 0, "\'major studies\'"),
    ("\'HIS\'", "\'History\'", 0, "\'major studies\'"),
    ("\'KIN\'", "\'Kinesiology\'", 0, "\'major studies\'"),
    ("\'LIB\'", "\'Liberal Studies\'", 0, "\'major studies\'"),
    ("\'MAT\'", "\'Mathematics\'", 0, "\'major studies\'"),
    ("\'MUS\'", "\'Music\'", 0, "\'major studies\'"),
    ("\'PHI\'", "\'Philosophy\'", 0, "\'major studies\'"),
    ("\'POL\'", "\'Political Science\'", 0, "\'major studies\'"),
    ("\'PSY\'", "\'Psychology\'", 0, "\'major studies\'"),
    ("\'PHY\'", "\'Physicis\'", 0, "\'major studies\'"),
    ("\'REG\'", "\'Religious Studies\'", 0, "\'major studies\'"),
    ("\'SOC\'", "\'Sociology\'", 0, "\'major studies\'"),
    ("\'SPA\'", "\'Spanish\'", 0, "\'major studies\'"),
    ("\'THE\'", "\'Theater Arts\'", 0, "\'major studies\'"),
    ("\'ADM\'", "\'Admissions and Records\'", 0, "\'affairs\'"),
    ("\'COU\'", "\'Counseling Center\'", 0, "\'affairs\'"),
    ("\'ALU\'", "\'Alumni Association\'", 0, "\'affairs\'"),
    ("\'HOU\'", "\'Housing Office\'", 0, "\'affairs\'"),
    ("\'DIS\'", "\'Disabilities Center\'", 0, "\'affairs\'")
]

#--uncomment to generate INT dept_id
for i in range(len(departments)):
    departments[i] = (i, departments[i][1], departments[i][2], departments[i][3])

people = []
roles = ["student", "staff", "faculty"]

for i in range(1000):
    firstName = faker.first_name()
    lastName = faker.last_name()
    role = faker.word(roles)
    people.append((
        #"\'{0:05d}".format(i) + role[:3] + "\'", #--use for VARCHAR person_id
        str(i),
        "\'" + firstName + "\'",
        "\'" + lastName + "\'",
        "\'888-951-{0:04d}".format(i) + "\'",
        "\'" + firstName + lastName + "@fictitous.edu\'",
        random.choice(buildings)[0],
        random.choice(departments)[0],
        "\'" + role + "\'"
    ))

for person in people:
    sqlFile.write(
        "INSERT INTO person VALUES (" + 
        str(person[0]) + ", " +
        person[1] + ", " +
        person[2] + ", " +
        person[3] + ", " +
        person[4] + ", " +
        str(person[5]) + ", " +
        str(person[6]) + "," + 
        person[7] + ");\n"
    )


for department in departments:
    department = (department[0], department[1], str(random.choice(people)[0]), department[3])
    sqlFile.write(
        "INSERT INTO department VALUES (" + 
        str(department[0]) + ", " +
        department[1] + ", " +
        department[2] + ", " +
        department[3] + ");\n"
    )


devices = []

for i in range(10000):
    devices.append((
        "\'DEV-{0:09d}\'".format(i), 
        "\'20" + str(random.randrange(10,21)) + "\'",
        "\'20" + str(random.randrange(10,21)) + "\'",
        random.choice(["true", "false"]),
        str(random.choice(departments)[0]),
        str(random.choice(rooms)[0]),
        str(random.choice(people)[0])
    ))

for device in devices:
    device_type = random.choice(random.choice([monitor_models, printer_models, projector_models, camera_models, computer_models, nwdevice_models, server_models]))[0]
    attr_monitor_id = "NULL, "
    attr_printer_id = "NULL, "
    attr_projector_id = "NULL, "
    attr_camera_id = "NULL, "
    attr_computer_id = "NULL, "
    attr_nwdevice_id = "NULL, "
    attr_server_id = "NULL, "

    if (device_type[1:4]=="MON"):
        attr_monitor_id == device_type
    elif (device_type[1:4]=="PRI"):
        attr_monitor_id == device_type
    elif (device_type[1:4]=="PRO"):
        attr_monitor_id == device_type
    elif (device_type[1:4]=="CAM"):
        attr_monitor_id == device_type
    elif (device_type[1:4]=="COM"):
        attr_monitor_id == device_type
    elif (device_type[1:4]=="NWD"):
        attr_monitor_id == device_type
    elif (device_type[1:4]=="SVR"):
        attr_monitor_id == device_type
    else:
        exit("YOU DONE GOOFED: " + str(device_type[1:4]))

    sqlFile.write(
        "INSERT INTO device (serial_number, year_manufac, date_purchased, active, monitor_id, printer_id, projector_id, camera_id, computer_id, nwdevice_id, server_id, dept_id, room_id, person_id) VALUES (" +
        device[0] + ", " +
        device[1] + ", " +
        device[2] + ", " +
        device[3] + ", " +
        attr_monitor_id +
        attr_printer_id +
        attr_projector_id +
        attr_camera_id +
        attr_computer_id +
        attr_nwdevice_id +
        attr_server_id +
        device[4] + ", " +
        device[5] + ", " +
        device[6] + ");\n"
    )

for server_model in server_models:
    sqlFile.write(
        "INSERT INTO server_model VALUES (" +
        server_model[0] + ", " +
        server_model[1] + ", " +
        server_model[2] + ", " +
        server_model[3] + ", " +
        server_model[4] + ", " +
        server_model[5] + ", " +
        server_model[6] + ");\n"
    )

for camera_model in camera_models:
    sqlFile.write(
        "INSERT INTO camera_model VALUES (" +
        camera_model[0] + ", " +
        camera_model[1] + ", " +
        camera_model[2] + ", " +
        camera_model[3] + ", " +
        camera_model[4] + ", " +
        camera_model[5] + ");\n"
    )

for projector_model in projector_models:
    sqlFile.write(
        "INSERT INTO projector_model VALUES (" +
        projector_model[0] + ", " +
        str(projector_model[1]) + ", " +
        projector_model[2] + ", " +
        str(projector_model[3]) + ", " +
        projector_model[4] + ", " +
        projector_model[5] + ", " +
        projector_model[6] + ");\n"
    )

for printer_model in printer_models:
    sqlFile.write(
        "INSERT INTO printer_model VALUES (" +
        printer_model[0] + ", " +
        printer_model[1] + ", " +
        printer_model[2] + ", " +
        str(printer_model[3]) + ", " +
        str(printer_model[4]) + ", " +
        printer_model[5] + ", " +
        str(printer_model[6]) + ", " +
        printer_model[7] + ");\n"
    )

for computer_model in computer_models:
    sqlFile.write(
        "INSERT INTO computer_model VALUES (" +
        computer_model[0] + ", " +
        computer_model[1] + ", " +
        computer_model[2] + ", " +
        computer_model[3] + ", " +
        computer_model[4] + ", " +
        computer_model[5] + ", " +
        computer_model[6] + ", " +
        computer_model[7] + ", " +
        computer_model[8] + ");\n"
    )

for monitor_model in monitor_models:
    sqlFile.write(
        "INSERT INTO monitor_model VALUES (" +
        monitor_model[0] + ", " +
        monitor_model[1] + ", " +
        monitor_model[2] + ", " +
        monitor_model[3] + ", " +
        monitor_model[4] + ", " +
        monitor_model[5] + ", " +
        monitor_model[6] + ");\n"
    )

for nwdevice_model in nwdevice_models:
    sqlFile.write(
        "INSERT INTO nwdevice_models VALUES (" +
        nwdevice_model[0] + ", " +
        nwdevice_model[1] + ", " +
        nwdevice_model[2] + ", " +
        nwdevice_model[3] + ", " +
        nwdevice_model[4] + ", " +
        nwdevice_model[5] + ", " +
        nwdevice_model[6] + ", " +
        nwdevice_model[7] + ", " +
        nwdevice_model[8] + ");\n"
    )