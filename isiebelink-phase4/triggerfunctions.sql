-- The function for part 4
DELIMITER //
CREATE PROCEDURE Delete_Model (IN parameter_model_id VARCHAR(64))
MODIFIES SQL DATA
BEGIN
    DELETE FROM device WHERE device.nwdevice_id=parameter_model_id; 
    DELETE FROM device WHERE device.monitor_id=parameter_model_id;
    DELETE FROM device WHERE device.printer_id=parameter_model_id;
    DELETE FROM device WHERE device.projector_id=parameter_model_id;
    DELETE FROM device WHERE device.camera_id=parameter_model_id;
    DELETE FROM device WHERE device.computer_id=parameter_model_id;
    DELETE FROM device WHERE device.server_id=parameter_model_id;

    SET @model_prefix = SUBSTRING(parameter_model_id, 1, 3);
    SET @model_table = CASE
        WHEN @model_prefix="NWD" THEN "nwdevice_model"
        WHEN @model_prefix="MON" THEN "monitor_model"
        WHEN @model_prefix="PRI" THEN "printer_model"
        WHEN @model_prefix="PRO" THEN "projector_model"
        WHEN @model_prefix="CAM" THEN "camera_model"
        WHEN @model_prefix="COM" THEN "computer_model"
        WHEN @model_prefix="SVR" THEN "server_model"
        ELSE "YOU DONE GOOFED"
    END;
    
    SET @prequrey = CONCAT("DELETE FROM ", @model_table, " WHERE ", @model_table, ".model_id=", @parameter_model_id, ";");
    PREPARE delete_query FROM @prequery;
    EXECUTE delete_query;
END //
DELIMITER;

-- The trigger for part 5
delimiter //
CREATE TRIGGER enforceSingleModelIDConstraint
   BEFORE INSERT ON device
   FOR EACH ROW
   BEGIN
      DECLARE multiple_model_ids CONDITION FOR SQLSTATE '45000';
      IF(
         countIfNotNull(NEW.monitor_id) +
         countIfNotNull(NEW.printer_id) +
         countIfNotNull(NEW.projector_id) +
         countIfNotNull(NEW.camera_id) +
         countIfNotNull(NEW.computer_id) +
         countIfNotNull(NEW.nwdevice_id) +
         countIfNotNull(NEW.server_id)
         > 1) THEN SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Warning: A row was not created. Device tuples must have only one non-null <model>_ID value.';
      END IF;
   END//
CREATE FUNCTION countIfNotNull(model_id VARCHAR(256)) RETURNS INT
BEGIN
   IF(model_id IS NULL OR model_id = "") THEN
      RETURN 0;
   ELSE
      RETURN 1;
   END IF;
END//
delimiter ;