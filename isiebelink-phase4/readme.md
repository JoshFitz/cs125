# Phase 4 - Josh Fitzgerald, Kevin Hodge, Isaac Sebelink, Frank Mugambage

Files in isiebelink-phase4, compiled by the authors:

1. JoshFitz-phase3 (authored by Josh Fitzgerald)
    - Folder that contains the contents of the last assignment, with some updates, per project specifications

    a. a.sql
        - This file contains 5 different queries that find different information within the database and includes different types of operations that SQL offers

    b. b.sql
        - This file contains 5 different data modifications statements, inluding 3 statements with subqueries

    c. c.sql
        - This file contains 2 different views and corresponding queries based on those views

    d. d.sql
        - This file contains 2 different indexes for aspects of the database that will be searched for extensively, along with explained queries using those indexes and corresponding drop statements

    e. readme.md
        - This file summarizes the purpose of each file

    f. JoshFitz-phase2
        - This folder contains the contents of phase 2, as specified below

        1. src (folder)
            - createLargeDump.py
                - This is the python script written primarily by Landon Moir that populates "populatelarge.sql" with the appropriate number of tuples with their corresponding types. This was made by importing the faker library.

        2. createschema.sql
            - This file is a modified version of the skeleton the class received from Professor Rodkey. We took out all the key declarations and changed the types to match realistic data inputs.

                CHANGES MADE TO CREATESCHEMA.SQL
                    - attribute based check contraint put on the attribute "phone" on table "person" to make sure it either only has 5 characters (for phone extensions) or 12 characters (for complete phone numbers)

                    - attribute based check constraint put on the attribute "hdmi" on table "monitor_model" to ensure no monitor has more than 5 hdmi ports since any more is not realistic

                    - tuple based check contraint on table "computer_model" that ensures every time a tuple is made or updated that the os either includes linux, mac, or windows

                    - keys added to all relations (almost all of them are the ids of their given relations) with foreign keys declared going from each device model_id to its device subclasses 
                    

        3. dropschema.sql
            - This file contains all the drop table commands.

        4. populatesmall.sql
            - Made primarily by Ben Arenchild and Josh Fitzgerald, this file contains approximately 10 tuples for each relation, with realstic yet fictitious data put into each relation using the INSERT command.

        5. populatelarge.sql
            - Made primarily by Landon Moir through the createLargeDump.py python script, this file had multiple relations with tens of thousands of tuples, several with thousands, and still several with hundreds. The contents of these tuples are not necessarily realistic yet still accomodates interactive queries.

        6. JoshFitzERdiagram.erdplus
            - This is the updated ER Diagram decided on by the class that our database was modeled after.

        7. readme.md
            - This file summarizes the purpose of each file

2. createschema.sql (adapted from JoshFitz-phase3/ by Josh Fitzgerald and Isaac Siebelink)
    - This file builds the database relations

3. dropschema.sql (adapted from JoshFitz-phase3/ by Josh Fitzgerald and Isaac Siebelink)
    - This file cleans out the database

4. populatesmall.sql (adapted from JoshFitz-phase3 by Josh Fitzgerald and Isaac Siebelink)
    - This file fills a structured database with a small dataset

5. readme.md
    - This file contains the contents and purposes of each folder and file included in the submission, and documents the changes made to past files in accordance with project specifications

6. triggerfunction.sql (authored by Isaac Siebelink and Kevin Hodge)
    - This file sets the trigger event and functions for the phase4 submission
    - Note: for some reason, I tried running my function and although the function was fine, the act of running it threw errors. I don't know why - the syntax *should* be flawless - so watch out
