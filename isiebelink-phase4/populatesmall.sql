INSERT INTO manufacturer
VALUES 
(1, "apple", "www.apple.com/help"),
(2, "microsoft", "www.microsoft.com/help"),
(3, "epson", "www.epson.com/help"),
(4, "GWsecurity", "www.GWsecurity.com/help"),
(5, "hp", "www.hp.com/help"),
(6, "Asus", "www.ASUS.com/help"),
(7, "Cisco", "www.cisco.com/help"),
(8, "Dell", "www.dell.com/help"),
(9, "Lenovo", "www.lenvo.com/help"),
(10, "ibm", "www.ibm.com/help");

INSERT INTO building
VALUES 
(1, "Lower", "VK", 200),
(2, "Lower", "Armington", 200),
(3, "Upper", "Emerson", 120),
(4, "Lower", "GLC", 200),
(5, "Off", "OV", 120),
(6, "Upper", "Clark", 150),
(7, "Upper", "page", 200),
(8, "Lower", "Deane", 80),
(9, "Upper", "Kerwood Hall", 50),
(10, "Lower", "Winter Hall", 200);

INSERT INTO person
VALUES
(1, "gayle", "beebe", "111,222,3333", "gbeebe@westmont.edu", 9, 2, "president"),
(2, "john", "doe", "222,222,3333", "jd@westmont.edu", 8, 4, "teacher"),
(3, "jane", "doe", "333,222,3333", "jdoe@westmont.edu", 8, 4, "prof"),
(4, "joe", "doe", "444,222,3333", "jodoe@westmont.edu", 10, 7, "professor"),
(5, "Josh", "Fitz", "111,233,3333", "jfitz@westmont.edu", 10, 9, "professor"),
(6, "will", "fitz", "111,222,4444", "wfitz@westmont.edu", 10, 8, "professor"),
(7, "blake", "fitz", "111,333,3333", "bfitz@westmont.edu", 9, 2, "receptionist"),
(8, "Nathan", "fitz", "111,222,5555", "nfitz@westmont.edu", 10, 7, "facult, NULLy"),
(9, "Elle", "fitz", "111,222,6666", "efitz@westmont.edu", 9, 1, "staff"),
(10, "Kendal", "fitz", "111,222,7777", "kfitz@westmont.edu", 9, 1, "staff");

INSERT INTO department
VALUES
(1, "Admissions", 10, "Admin"),
(2, "President", 16, "Admin"),
(3, "Provost", 200, "Provost"),
(4, "EB", 289, "Academic"),
(5, "Registrar", 12, "Admin"),
(6, "DA", 46, "Academic"),
(7, "MA", 46, "Academic"),
(8, "CS", 46, "Academic"),
(9, "Physics", 98, "Academic"),
(10, "IT", 87, "Admin");

INSERT INTO room
VALUES 
(1, "Study Lounge. In need of a new printer.", 1),
(2, "C Lounge", 2),
(3, "RD Office", 3),
(4, "North Kitchen", 4),
(5, "Lounge", 5),
(6, "A Lounge", 6),
(7, "Multi-Purpose Room", 7),
(8, "Office", 8),
(9, "Admissions Office", 9),
(10, "Computer Room", 10);

INSERT INTO rack
VALUES
(1, 1, 10),
(2, 2, 10),
(3, 3, 10),
(4, 4, 10),
(5, 5, 10),
(6, 6, 10),
(7, 7, 10),
(8, 8, 10),
(9, 9, 10),
(10, 10, 10);

INSERT INTO server_model
VALUES 
("SVR-00", "10", "2", "macOS", "10", TRUE, 8),
("SVR-01", "5", "4", "macOS", "5", FALSE, 8),
("SVR-02", "7", "5", "macOS", "6", TRUE, 8),
("SVR-03", "10", "10", "macOS", "8", FALSE, 8),
("SVR-04", "9", "8", "macOS", "10", TRUE, 8),
("SVR-05", "4", "6", "macOS", "10", FALSE, 8),
("SVR-06", "12", "12", "macOS", "12", TRUE,8),
("SVR-07", "14", "10", "macOS", "10", FALSE, 8),
("SVR-08", "12", "7", "macOS", "14", TRUE, 8),
("SVR-09", "11", "8", "macOS", "10", FALSE, 8);

INSERT INTO camera_model
VALUES
("CAM-00", TRUE, 12, "1080p", 60, 4),
("CAM-01", FALSE, 40, "1080p", 98, 4),
("CAM-02", TRUE, 12, "1440p", 99, 4),
("CAM-03", FALSE, 30, "1920p", 90, 4),
("CAM-04", TRUE, 10, "1080p", 80, 4),
("CAM-05", TRUE, 22, "8MP", 75, 4),
("CAM-06", FALSE, 33, "1080p", 75, 4),
("CAM-07", TRUE, 15, "1440p", 150, 4),
("CAM-08", FALSE, 19, "1920p", 100, 4),
("CAM-09", TRUE, 15, "1080p", 90, 4);

INSERT INTO projector_model
VALUES
("PRO-00", 4, TRUE, 2300, TRUE, "1920x1700", 3),
("PRO-01", 1, FALSE, 2500, FALSE, "1820x1800", 3),
("PRO-02", 2, TRUE, 2300, FALSE, "1920x1700", 3),
("PRO-03", 3, FALSE, 4000, TRUE, "1720x1900", 3),
("PRO-04", 4, FALSE, 2500, FALSE, "1940x1300", 3),
("PRO-05", 3, FALSE, 3000, FALSE, "1820x1700", 3),
("PRO-06", 2, TRUE, 4000, TRUE, "1820x1900", 3),
("PRO-07", 1, FALSE, 3500, TRUE, "1990x1300", 3),
("PRO-08", 2, TRUE, 4000, TRUE, "1870x1400", 3),
("PRO-09", 1, FALSE, 2500, FALSE, "1980x2800", 3);

INSERT INTO printer_model
VALUES
("PRI-00", FALSE, "8x11", 12, 21, TRUE , "Color", 5),
("PRI-01", TRUE, "8x11", 12, 20, TRUE, "BW", 5),
("PRI-02", FALSE, "10x8", 20, 12, FALSE, "Color", 5),
("PRI-03", TRUE, "5x7", 10, 21, TRUE, "BW", 5),
("PRI-04", FALSE, "8x11", 12, 20, FALSE, "BW", 5),
("PRI-05", TRUE, "8x8", 7, 29, TRUE, "BW", 5),
("PRI-06", TRUE, "8x11", 10, 28, FALSE, "Color", 5),
("PRI-07", TRUE, "7x5", 8, 27, FALSE, "Color", 5),
("PRI-08", FALSE, "9x12", 9, 28, TRUE, "Color", 5),
("PRI-09", TRUE, "8x11", 10, 28, TRUE, "Color", 5);

INSERT INTO computer_model
VALUES
("COM-00", "Intel Core i5-12600K", 15, "1200x800", 16, "macOS", "apple", 250, 1),
("COM-01", "AMD Ryzen 7 5700G", 14, "1200x800", 8, "windows", "microsoft", 250, 2),
("COM-02", "Intel Core i5-10400", 15, "1200x800", 32, "macOS", "apple", 500, 1),
("COM-03", "AMD Ryzen 3 3300X", 10, "1200x800", 4, "linux", "dell", 80, 2),
("COM-04", "AMD Ryzen 9 5900X", 11, "1200x800", 16, "macOS", "apple", 250, 1),
("COM-05", "Intel Core i9-12900K", 15, "1200x800", 8, "windows", "microsoft", 100, 2),
("COM-06", "Intel Core i5-12600K", 13, "1200x800", 4, "linux", "dell", 50, 1),
("COM-07", "Intel Core i5-11600K", 13, "1200x800", 8, "macOS", "apple", 80, 2),
("COM-08", "AMD Ryzen 5 5600X", 15, "1200x800", 16, "linux", "dell", 150, 1),
("COM-09", "AMD Ryzen 5 5600X", 12, "1200x800", 16, "macOS", "apple", 120, 2);

INSERT INTO monitor_model
VALUES
("MON-00", 2, "OLED", "1920x1080", 32, 60, 6),
("MON-01", 2, "TN", "1366x768", 24, 75, 6),
("MON-02", 3, "OLED", "1366x768", 24, 60, 6),
("MON-03", 3, "OLED", "1920x1080", 30, 144, 6),
("MON-04", 2, "VA", "360x800", 30, 240, 6),
("MON-05", 3, "IPS", "1366x768", 24, 60, 6),
("MON-06", 1, "TN", "1536×864", 24, 75, 6),
("MON-07", 3, "VA", "414x896", 28, 60, 6),
("MON-08", 2, "OLED", "360x640", 32, 60, 6),
("MON-09", 2, "OLED", "1920x1080", 28, 144, 6);

INSERT INTO nwdevice_model
VALUES
("NWD-00", "0F:13:27:76:50:B4", "ipv4", "Access Point", 1.8, 1024, 47, 100, 7),
("NWD-01", "00:A9:39:14:B8:5B", "ipv4", "Access Point", 3.5, 1025, 100, 150, 7),
("NWD-02", "54:0B:D7:59:AB:D5", "ipv4", "Access Point", 1.8, 1026, 150, 80, 7),
("NWD-03", "F0:2F:15:E4:54:E7", "ipv6", "Access Point", 1.8, 1027, 250, 60, 7),
("NWD-04", "E6:4F:F6:C0:1F:AA", "ipv4", "Access Point", 3.5, 1028, 200, 100, 7),
("NWD-05", "AB:79:D1:86:28:B4", "ipv4", "Access Point", 4.0, 1029, 150, 120, 7),
("NWD-06", "6B:BA:01:D5:F7:5B", "ipv4", "Access Point", 1.8, 1030, 100, 70, 7),
("NWD-07", "8B:7F:40:42:1A:B3", "ipv6", "Router", 4.0, 1031, 300, 100, 7),
("NWD-08", "32:33:BB:2C:0A:7E", "ipv6", "Access Point", 1.8, 1032, 230, 150, 7),
("NWD-09", "A1:90:92:19:E5:D4", "ipv4", "Access Point", 4.0, 1033, 160, 100, 7);

INSERT INTO device
VALUES
("2xfgneng3h19j6q8onal0tnpzjMAZJ9rkZM", NULL, NULL, NULL, 2009, "2010-04-09", NULL, TRUE, "MON-00", NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL),
("1rc7k025rf4qqaucsi67k2om1Q94hteMsdh", NULL, NULL, NULL, 2008, "2010-12-18", NULL, TRUE, "MON-01", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5),
("2dzydcoz5ybgaktoxvoi1odflaX8hfrGYR2", NULL, NULL, NULL, 2012, "2014-05-28", NULL, TRUE, "MON-02", NULL, NULL, NULL, NULL, NULL, NULL, 10, NULL, NULL, NULL),
("9nti9aetkeqzybh95hw5wqe0o7FuXxDju2e", NULL, NULL, NULL, 2020, "2021-11-18", NULL, TRUE, "MON-03", NULL, NULL, NULL, NULL, NULL, NULL, 9, NULL, NULL, NULL),
("4yvphjd71k4o50yzqhxq7uqilvhRvEQSU9f", NULL, NULL, NULL, 2020, "2020-08-31", NULL, TRUE, "MON-04", NULL, NULL, NULL, NULL, NULL, NULL, 10, NULL, NULL, NULL),
("8nekq18u8nm9p9e1iym3jrk8hTW5RvYLqJk", NULL, NULL, NULL, 2019, "2021-05-03", NULL, TRUE, "MON-05", NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, NULL),
("8t5o93mvjsf8a7j5fgz28d90vM6DwduZFbF", NULL, NULL, NULL, 2012, "2013-09-16", NULL, FALSE, "MON-06", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
("ag6eofaiy64e2oluyrr74fwc3qPLdBqGQur", NULL, NULL, NULL, 2019, "2020-02-13", NULL, TRUE, "MON-07", NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, NULL, NULL),
("a9sn8r7qeeopmo0u0pllqqh7fLvbgrDm7ri", NULL, NULL, NULL, 2015, "2017-01-09", NULL, TRUE, "MON-08", NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 9),
("47y2joljjrn8y1utfzlxcdir6nFBty8g9sx", NULL, NULL, NULL, 2014, "2018-11-06", NULL, TRUE, "MON-09", NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, NULL, NULL),
("0fvm5h62ykob8f7ukyopdj9q5jMAZJ9rkZM", NULL, NULL, NULL, 2018, "2019-07-26", NULL, TRUE, NULL, "PRI-00", NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL),
("ep46n57eoplj3cr7oelczaolyQ94hteMsdh", NULL, NULL, NULL, 2018, "2021-04-29", NULL, TRUE, NULL, "PRI-01", NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL),
("22pz150esui9dgahptr68wautaX8hfrGYR2", NULL, NULL, NULL, 2021, "2021-07-20", NULL, TRUE, NULL, "PRI-02", NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL),
("bj0a6b6wh3ydsilg0rije2vxb7FuXxDju2e", NULL, NULL, NULL, 2020, "2020-12-24", NULL, TRUE, NULL, "PRI-03", NULL, NULL, NULL, NULL, NULL, NULL, 5, NULL, NULL),
("3tsrvbzoienijcvvekxywx7v0vhRvEQSU9f", NULL, NULL, NULL, 2019, "2018-02-28", NULL, TRUE, NULL, "PRI-04", NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, NULL),
("1hb6cb00wyq4tw892n6ypsb5iTW5RvYLqJk", NULL, NULL, NULL, 2015, "2019-01-18", NULL, TRUE, NULL, "PRI-05", NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL, NULL),
("bu5h5zfi32ybq4hd5ad2bvzftM6DwduZFbF", NULL, NULL, NULL, 2012, "2018-08-11", NULL, TRUE, NULL, "PRI-06", NULL, NULL, NULL, NULL, NULL, NULL, 9, NULL, NULL),
("9q190tu3mlo9gn2ybg0hvpchyqPLdBqGQur", NULL, NULL, NULL, 2015, "2017-04-10", NULL, TRUE, NULL, "PRI-07", NULL, NULL, NULL, NULL, NULL, NULL, 10, NULL, NULL),
("b0xj841tvdk3iek1ztvfqnnyeLvbgrDm7ri", NULL, NULL, NULL, 2020, "2020-11-09", NULL, FALSE, NULL, "PRI-08", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
("26pz12e6169p5f5gr1r249n60nFBty8g9sx", NULL, NULL, NULL, 2015, "2016-10-31", NULL, FALSE, NULL, "PRI-09", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
("6cjdmkyo6h8ncdjpch291x89pjMAZJ9rkZM", NULL, NULL, NULL, 2018, "2020-03-08", NULL, TRUE, NULL, NULL, "PRO-00", NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL),
("95isl7mz8vltmdwt5q0x075nlQ94hteMsdh", NULL, NULL, NULL, 2017, "2019-03-04", NULL, TRUE, NULL, NULL, "PRO-01", NULL, NULL, NULL, NULL, NULL, 5, NULL, NULL),
("3y4izi5obblp8gu3m2h4pst4saX8hfrGYR2", NULL, NULL, NULL, 2019, "2020-03-27", NULL, TRUE, NULL, NULL, "PRO-02", NULL, NULL, NULL, NULL, NULL, 6, NULL, NULL),
("8l8qigednqo7pa7krn94ls2q77FuXxDju2e", NULL, NULL, NULL, 2020, "2021-05-02", NULL, TRUE, NULL, NULL, "PRO-03", NULL, NULL, NULL, NULL, NULL, 7, NULL, NULL),
("c6q2vnnyxi5u0gjyf4vptkyudvhRvEQSU9f", NULL, NULL, NULL, 2014, "2017-10-22", NULL, FALSE, NULL, NULL, "PRO-04", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
("elorczffg7roqosb8i2kqye52TW5RvYLqJk", NULL, NULL, NULL, 2013, "2014-04-19", NULL, TRUE, NULL, NULL, "PRO-05", NULL, NULL, NULL, NULL, NULL, 10, 8, NULL),
("3hr5vw34hy0i06qh4wrw3btkbM6DwduZFbF", NULL, NULL, NULL, 2012, "2014-01-09", NULL, FALSE, NULL, NULL, "PRO-06", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
("2zo2mo7lqajmopjy3ox67pr2aqPLdBqGQur", NULL, NULL, NULL, 2019, "2020-03-30", NULL, FALSE, NULL, NULL, "PRO-07", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
("ejheaxdwuedbp6nb04py22ul2LvbgrDm7ri", NULL, NULL, NULL, 2017, "2019-02-06", NULL, FALSE, NULL, NULL, "PRO-08", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
("8zcfvqhbvjyni1p73xyf88pvznFBty8g9sx", NULL, NULL, NULL, 2018, "2019-06-14", NULL, FALSE, NULL, NULL, "PRO-09", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
("48b36f8duxk90f4boselwbxcfjMAZJ9rkZM", NULL, NULL, NULL, 2013, "2016-08-03", NULL, TRUE, NULL, NULL, NULL, "CAM-00", NULL, NULL, NULL, 10, NULL, NULL, NULL),
("9rptox82u6s0grupxgwypuy0xQ94hteMsdh", NULL, NULL, NULL, 2016, "2020-02-03", NULL, TRUE, NULL, NULL, NULL, "CAM-01", NULL, NULL, NULL, 10, NULL, NULL, NULL),
("7u7jxjx5zvts2urdeqbwsdepmaX8hfrGYR2", NULL, NULL, NULL, 2012, "2016-11-24", NULL, TRUE, NULL, NULL, NULL, "CAM-02", NULL, NULL, NULL, 10, NULL, NULL, NULL),
("a8vaw0znuqi2dnw1i1yvbbjs27FuXxDju2e", NULL, NULL, NULL, 2020, "2021-12-16", NULL, TRUE, NULL, NULL, NULL, "CAM-03", NULL, NULL, NULL, 10, NULL, NULL, NULL),
("bzb1uqejkddsd4eax9l4kqqsvvhRvEQSU9f", NULL, NULL, NULL, 2020, "2020-04-20", NULL, TRUE, NULL, NULL, NULL, "CAM-04", NULL, NULL, NULL, 10, NULL, NULL, NULL),
("dyucqr4wk7hr51aluy7bok62gTW5RvYLqJk", NULL, NULL, NULL, 2007, "2010-09-28", NULL, FALSE, NULL, NULL, NULL, "CAM-05", NULL, NULL, NULL, NULL, NULL, NULL, NULL),
("enl3gn2p52ge4j71hz1oxr6ybM6DwduZFbF", NULL, NULL, NULL, 2012, "2012-03-19", NULL, TRUE, NULL, NULL, NULL, "CAM-06", NULL, NULL, NULL, 10, NULL, NULL, NULL),
("91txcohk7nvyd226koln8lsg0qPLdBqGQur", NULL, NULL, NULL, 2015, "2017-06-02", NULL, TRUE, NULL, NULL, NULL, "CAM-07", NULL, NULL, NULL, 10, NULL, NULL, NULL),
("5byhqmz9cp4uirvmrnaec0qjuLvbgrDm7ri", NULL, NULL, NULL, 2013, "2014-08-04", NULL, TRUE, NULL, NULL, NULL, "CAM-08", NULL, NULL, NULL, 10, NULL, NULL, NULL),
("0p1tlfe6hsqfbkiqwz6ba4r1qnFBty8g9sx", NULL, NULL, NULL, 2021, "2021-01-31", NULL, TRUE, NULL, NULL, NULL, "CAM-09", NULL, NULL, NULL, 10, NULL, NULL, NULL),
("7z41qp0mcfx8h5ncm34y7stjnjMAZJ9rkZM", NULL, NULL, NULL, 2018, "2018-08-28", "windows", TRUE, NULL, NULL, NULL, NULL, "COM-00", NULL, NULL, 7, NULL, NULL, NULL),
("e83hi05qa9cfgtc878qx3pbxaQ94hteMsdh", NULL, NULL, NULL, 2016, "2016-02-12", "windows", TRUE, NULL, NULL, NULL, NULL, "COM-01", NULL, NULL, NULL, NULL, NULL, 4),
("6pvpyv0gfiw7qb6rbewj71pataX8hfrGYR2", NULL, NULL, NULL, 2012, "2012-03-22", "linux", FALSE, NULL, NULL, NULL, NULL, "COM-02", NULL, NULL, NULL, NULL, NULL, NULL),
("at5209vy4u3m9l06sehvhwv617FuXxDju2e", NULL, NULL, NULL, 2020, "2021-11-15", "linux", TRUE, NULL, NULL, NULL, NULL, "COM-03", NULL, NULL, 9, NULL, NULL, NULL),
("590ezgcv29fdsloruqw77hqrgvhRvEQSU9f", NULL, NULL, NULL, 2020, "2020-04-15", "windows", TRUE, NULL, NULL, NULL, NULL, "COM-04", NULL, NULL, NULL, 5, NULL, NULL),
("3frnmkkcdk1yzmmd3f4p2ap6qTW5RvYLqJk", NULL, NULL, NULL, 2015, "2017-02-11", "linux", FALSE, NULL, NULL, NULL, NULL, "COM-05", NULL, NULL, NULL, NULL, NULL, NULL),
("d1lvzvagexaxv47t7ht9ndt4vM6DwduZFbF", NULL, NULL, NULL, 2012, "2015-01-04", "macOS", TRUE, NULL, NULL, NULL, NULL, "COM-06", NULL, NULL, 8, NULL, NULL, NULL),
("54lea8evofuavwl8u97gal49pqPLdBqGQur", NULL, NULL, NULL, 2017, "2018-03-30", "macOS", TRUE, NULL, NULL, NULL, NULL, "COM-07", NULL, NULL, NULL, NULL, NULL, 6),
("32verlhlsphpxqioaof66ri03LvbgrDm7ri", NULL, NULL, NULL, 2021, "2021-01-23", "windows", TRUE, NULL, NULL, NULL, NULL, "COM-08", NULL, NULL, NULL, NULL, NULL, 9),
("74cfguhg6xzmvubpu0zxnk4nqnFBty8g9sx", NULL, NULL, NULL, 2021, "2021-10-07", "linux", TRUE, NULL, NULL, NULL, NULL, "COM-09", NULL, NULL, 3, NULL, NULL, NULL),
("6c5r8jc25200ihg19fu7sebewjMAZJ9rkZM", NULL, NULL, NULL, 2018, "2020-06-01", NULL, TRUE, NULL, NULL, NULL, NULL, NULL, "NWD-00", NULL, NULL, 1, NULL, NULL),
("ab99usnzoww7omzswacyijugkQ94hteMsdh", NULL, NULL, NULL, 2016, "2016-10-24", NULL, TRUE, NULL, NULL, NULL, NULL, NULL, "NWD-01", NULL, NULL, 2, NULL, NULL),
("9uagm1xrnuc609dqjqwmdnmzdaX8hfrGYR2", NULL, NULL, NULL, 2009, "2010-04-10", NULL, TRUE, NULL, NULL, NULL, NULL, NULL, "NWD-02", NULL, NULL, 3, NULL, NULL),
("20fzn6yxd34dbyguua30ym66r7FuXxDju2e", NULL, NULL, NULL, 2014, "2015-09-18", NULL, TRUE, NULL, NULL, NULL, NULL, NULL, "NWD-03", NULL, NULL, 4, NULL, NULL),
("bo4g7wonx7uh57hw2ol26ly79vhRvEQSU9f", NULL, NULL, NULL, 2020, "2020-04-01", NULL, TRUE, NULL, NULL, NULL, NULL, NULL, "NWD-04", NULL, NULL, 5, NULL, NULL),
("6a71pzepo35ahioaxrczh7ur8TW5RvYLqJk", NULL, NULL, NULL, 2012, "2013-01-10", NULL, TRUE, NULL, NULL, NULL, NULL, NULL, "NWD-05", NULL, NULL, 6, NULL, NULL),
("a1e1qg8dlslh4lufu8clop884M6DwduZFbF", NULL, NULL, NULL, 2012, "2018-06-13", NULL, TRUE, NULL, NULL, NULL, NULL, NULL, "NWD-06", NULL, NULL, 7, NULL, NULL),
("2gmdugurnz2jqlhken17f03y2qPLdBqGQur", NULL, NULL, NULL, 2019, "2019-02-20", NULL, TRUE, NULL, NULL, NULL, NULL, NULL, "NWD-07", NULL, NULL, 8, NULL, NULL),
("5gesg2j25240rukgh2nmxnkmfLvbgrDm7ri", NULL, NULL, NULL, 2021, "2021-10-03", NULL, TRUE, NULL, NULL, NULL, NULL, NULL, "NWD-08", NULL, NULL, 9, NULL, NULL),
("0cdmefcmr8isqxcs0qlc8phisnFBty8g9sx", NULL, NULL, NULL, 2019, "2019-12-04", NULL, TRUE, NULL, NULL, NULL, NULL, NULL, "NWD-09", NULL, NULL, 10, NULL, NULL),
("1xry3byxk9wjoozwki112s696jMAZJ9rkZM", NULL, NULL, NULL, 2018, "2018-12-27", NULL, FALSE, NULL, NULL, NULL, NULL, NULL, NULL, "SVR-00", NULL, NULL, 1, NULL),
("1pfsapir505bg0od68lxv50wnQ94hteMsdh", NULL, NULL, NULL, 2007, "2008-03-31", NULL, TRUE, NULL, NULL, NULL, NULL, NULL, NULL, "SVR-01", NULL, NULL, 2, NULL),
("52lzcphnxvavefdwi1tikqap7aX8hfrGYR2", NULL, NULL, NULL, 2012, "2012-05-20", NULL, TRUE, NULL, NULL, NULL, NULL, NULL, NULL, "SVR-02", NULL, NULL, 3, NULL),
("ck95kx3vb0uthtqajmf8iq97h7FuXxDju2e", NULL, NULL, NULL, 2020, "2021-01-29", NULL, TRUE, NULL, NULL, NULL, NULL, NULL, NULL, "SVR-03", NULL, NULL, 4, NULL),
("ddboh1xo89dqpdqpoqpl55ekpvhRvEQSU9f", NULL, NULL, NULL, 2020, "2020-08-14", NULL, TRUE, NULL, NULL, NULL, NULL, NULL, NULL, "SVR-04", NULL, NULL, 5, NULL),
("1za209jxox1qepu52k3d1hub7TW5RvYLqJk", NULL, NULL, NULL, 2015, "2017-01-10", NULL, FALSE, NULL, NULL, NULL, NULL, NULL, NULL, "SVR-05", NULL, NULL, 6, NULL),
("6jpl5e4zb1ilmv9qnbs3afiz2M6DwduZFbF", NULL, NULL, NULL, 2012, "2017-03-09", NULL, TRUE, NULL, NULL, NULL, NULL, NULL, NULL, "SVR-06", NULL, NULL, 7, NULL),
("72i6ltptfx4d2q9dewjo23izpqPLdBqGQur", NULL, NULL, NULL, 2013, "2014-10-24", NULL, FALSE, NULL, NULL, NULL, NULL, NULL, NULL, "SVR-07", NULL, NULL, 8, NULL),
("emkuadbjc4qxvi7oqpdelbdebLvbgrDm7ri", NULL, NULL, NULL, 2021, "2021-11-04", NULL, TRUE, NULL, NULL, NULL, NULL, NULL, NULL, "SVR-08", NULL, NULL, 9, NULL),
("299u22efdi1ihwp41xs7hvrlnnFBty8g9sx", NULL, NULL, NULL, 2021, "2021-09-17", NULL, TRUE, NULL, NULL, NULL, NULL, NULL, NULL, "SVR-09", NULL, NULL, 10, NULL);