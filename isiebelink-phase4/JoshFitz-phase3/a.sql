/* Question: What building is the oldest device located in? */
/* Query 1 */
SELECT D1.serial_number, D1.year_manufac, building.name 
FROM room, building, owner_of, device D1
WHERE room.bldg_id = building.bldg_id 
AND owner_of.room_id = room.room_id 
AND owner_of.serial_number = D1.serial_number
AND D1.active 
AND D1.year_manufac <= ALL(SELECT year_manufac
                            FROM device, owner_of 
                            WHERE device.serial_number = owner_of.serial_number
                            AND active);


/* Question: What is the most recent computer that Westmont has purchased that has yet to be assigned to someone? */
/* Query 2 */ 
SELECT D.serial_number, D.date_purchased
FROM device D, computer_model C
WHERE D.computer_id = C.model_id
AND active = false
AND D.date_purchased > ALL(SELECT date_purchased
                            FROM device, computer_model
                            WHERE device.computer_id = computer_model.model_id);


/* Question: How many people can the Access Point with the biggest coverage provide service to? */
/* Query 3 */
SELECT model_id, type, number_clients, MAX(coverage)
FROM nwdevice_model
WHERE type = "access point";


/* Question: What is the average RAM of all active computers? */
/* Query 4 */
SELECT AVG(computer_model.ram)
FROM computer_model JOIN device ON computer_model.model_id = device.computer_id
WHERE device.active;


/* Question: What is the amount of HDMI ports for the projectors that have speakers on campus? */
/* Query 5 */
SELECT model_id, hdmi
FROM projector_model
WHERE speakers
ORDER BY hdmi desc;