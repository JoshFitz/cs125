INSERT INTO manufacturer
VALUES 
(1, "apple", "www.apple.com/help"),
(2, "microsoft", "www.microsoft.com/help"),
(3, "epson", "www.epson.com/help"),
(4, "GWsecurity", "www.GWsecurity.com/help"),
(5, "hp", "www.hp.com/help"),
(6, "Asus", "www.ASUS.com/help"),
(7, "Cisco", "www.cisco.com/help"),
(8, "Dell", "www.dell.com/help"),
(9, "Lenovo", "www.lenvo.com/help"),
(10, "ibm", "www.ibm.com/help");
INSERT INTO building
VALUES 
(1, "lower", "VK", 10),
(2, "Lower", "Armington", 10),
(3, "Upper", "Emerson", 20),
(4, "lower", "GLC", 100),
(5, "Off", "OV", 24),
(6, "Upper", "Clark", 15),
(7, "Upper", "page", 20),
(8, "lower", "Deane", 30),
(9, "lower", "Music", 20),
(10, "Upper", "Mail Center", 10);
INSERT INTO person
VALUES
(1, "gayle", "beebe", "111,222,3333", "gbeebe@westmont.edu", "president"),
(2, "john", "doe", "222,222,3333", "jd@westmont.edu", "teacher"),
(3, "jane", "doe", "333,222,3333", "jdoe@westmont.edu", "prof"),
(4, "joe", "doe", "444,222,3333", "jodoe@westmont.edu", "student"),
(5, "Josh", "Fitz", "111,233,3333", "jfitz@westmont.edu", "student"),
(6, "will", "fitz", "111,222,4444", "wfitz@westmont.edu", "student"),
(7, "blake", "fitz", "111,333,3333", "bfitz@westmont.edu", "professor"),
(8, "Nathan", "fitz", "111,222,5555", "nfitz@westmont.edu", "faculty"),
(9, "Elle", "fitz", "111,222,6666", "efitz@westmont.edu", "student"),
(10, "Kendal", "fitz", "111,222,7777", "kfitz@westmont.edu", "student");
INSERT INTO department
VALUES
(1, "EB", 10, "Academic"),
(2, "President", 16, "Admin"),
(3, "Provost", 200, "Provost"),
(4, "Philosophy", 289, "Academic"),
(5, "Registrar", 12, "Admin"),
(6, "DA", 46, "Academic"),
(7, "MA", 46, "Academic"),
(8, "CS", 46, "Academic"),
(9, "Physics", 98, "Academic"),
(10, "Art", 87, "academic");
INSERT INTO server_model
VALUES 
(1, "10", "2", "macOS", "10", TRUE, 8),
(2, "5", "4", "macOS", "5", FALSE, 8),
(3, "7", "5", "macOS", "6", TRUE, 8),
(4, "10", "10", "macOS", "8", FALSE, 8),
(5, "9", "8", "macOS", "10", TRUE, 8),
(6, "4", "6", "macOS", "10", FALSE, 8),
(7, "12", "12", "macOS", "12", TRUE,8),
(8, "14", "10", "macOS", "10", FALSE, 8),
(9, "12", "7", "macOS", "14", TRUE, 8),
(10, "11", "8", "macOS", "10", FALSE, 8);
INSERT INTO works_in
VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6),
(7, 7),
(8, 8),
(9, 9),
(10, 10); 
INSERT INTO member_of
VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6),
(7, 7),
(8, 8),
(9, 9),
(10, 10);
INSERT INTO camera_model
VALUES
(TRUE, 12, "High", 60, 11, 4),
(FALSE, 40, "medium", 98, 12, 4),
(TRUE, 12, "High", 99, 13, 4),
(FALSE, 30, "low", 90, 14, 4),
(TRUE, 10, "High", 80, 15, 4),
(TRUE, 22, "medium", 75, 16, 4),
(FALSE, 33, "High", 75, 17, 4),
(TRUE, 15, "low", 150, 18, 4),
(FALSE, 19, "High", 100, 19, 4),
(TRUE, 15, "medium", 90, 20, 4);
INSERT INTO projector_model
VALUES
(21, 4, TRUE, 2300, TRUE, "1920x1700", 3),
(22, 1, FALSE, 2500, FALSE, "1820x1800", 3),
(23, 2, TRUE, 2300, FALSE, "1920x1700", 3),
(24, 3, FALSE, 4000, TRUE, "1720x1900", 3),
(25, 4, FALSE, 2500, FALSE, "1940x1300", 3),
(26, 3, FALSE, 3000, FALSE, "1820x1700", 3),
(27, 2, TRUE, 4000, TRUE, "1820x1900", 3),
(28, 1, FALSE, 3500, TRUE, "1990x1300", 3),
(29, 2, TRUE, 4000, TRUE, "1870x1400", 3),
(30, 1, FALSE, 2500, FALSE, "1980x2800", 3);
INSERT INTO printer_model
VALUES
(31, FALSE, "8x11", 12, 21, TRUE , "Color", 5),
(32, TRUE, "8x11", 12, 20, TRUE, "BW", 5),
(33, FALSE, "10x8", 20, 12, FALSE, "Color", 5),
(34, TRUE, "5x7", 10, 21, TRUE, "BW", 5),
(35, FALSE, "8x11", 12, 20, FALSE, "BW", 5),
(36, TRUE, "8x8", 7, 29, TRUE, "BW", 5),
(37, TRUE, "8x11", 10, 28, FALSE, "Color", 5),
(38, TRUE, "7x5", 8, 27, FALSE, "Color", 5),
(39, FALSE, "9x12", 9, 28, TRUE, "Color", 5),
(40, TRUE, "8x11", 10, 28, TRUE, "Color", 5);
INSERT INTO computer_model
VALUES
(41, "3", 15, "1200x800", 8, "macOS", "apple", 2.5, 1),
(42, "4", 14, "1200x800", 8, "windows", "microsoft", 2.0, 2),
(43, "8", 15, "1200x800", 8, "macOS", "apple", 3.5, 1),
(44, "7", 10, "1200x800", 8, "linux", "dell", 2.0, 2),
(45, "6", 11, "1200x800", 8, "macOS", "apple", 3.5, 1),
(46, "5", 15, "1200x800", 8, "windows", "microsoft", 2.5, 2),
(47, "5", 13, "1200x800", 8, "linux", "dell", 3.5, 1),
(48, "4", 13, "1200x800", 8, "macOS", "apple", 3.0, 2),
(49, "3", 15, "1200x800", 8, "linux", "dell", 3.5, 1),
(50, "3", 12, "1200x800", 8, "macOS", "apple", 2.0, 2);
INSERT INTO monitor_model
VALUES
(51, 2, "OLED", "1920x1080", 32, 60, 6),
(52, 2, "TN", "1366x768", 24, 75, 6),
(53, 3, "OLED", "1366x768", 24, 60, 6),
(54, 3, "OLED", "1920x1080", 30, 144, 6),
(55, 2, "VA", "360x800", 30, 240, 6),
(56, 3, "IPS", "1366x768", 24, 60, 6),
(57, 1, "TN", "1536×864", 24, 75, 6),
(58, 3, "VA", "414x896", 28, 60, 6),
(59, 2, "OLED", "360x640", 32, 60, 6),
(60, 2, "OLED", "1920x1080", 28, 144, 6);
INSERT INTO nwdevice_model
VALUES
(61, "0F:13:27:76:50:B4", "ipv4", "Access Point", 1.8, 1024, 47, 100, 7),
(62, "00:A9:39:14:B8:5B", "ipv4", "Access Point", 3.5, 1025, 100, 150, 7),
(63, "54:0B:D7:59:AB:D5", "ipv4", "Access Point", 1.8, 1026, 150, 80, 7),
(64, "F0:2F:15:E4:54:E7", "ipv6", "Access Point", 1.8, 1027, 250, 60, 7),
(65, "E6:4F:F6:C0:1F:AA", "ipv4", "Access Point", 3.5, 1028, 200, 100, 7),
(66, "AB:79:D1:86:28:B4", "ipv4", "Access Point", 4.0, 1029, 150, 120, 7),
(67, "6B:BA:01:D5:F7:5B", "ipv4", "Access Point", 1.8, 1030, 100, 70, 7),
(68, "8B:7F:40:42:1A:B3", "ipv6", "Router", 4.0, 1031, 300, 100, 7),
(69, "32:33:BB:2C:0A:7E", "ipv6", "Access Point", 1.8, 1032, 230, 150, 7),
(70, "A1:90:92:19:E5:D4", "ipv4", "Access Point", 4.0, 1033, 160, 100, 7);
INSERT INTO device
VALUES
("jMAZJ9rkZM", NULL, NULL, NULL, 2018, "2018-08-21", NULL, TRUE, 53, NULL, NULL, NULL, NULL, NULL, NULL),
("Q94hteMsdh", NULL, NULL, NULL, 2016, "2016-02-03", NULL, TRUE, NULL, 33, NULL, NULL, NULL, NULL, NULL),
("aX8hfrGYR2", NULL, NULL, NULL, 2012, "2012-03-24", NULL, TRUE, NULL, NULL, 29, NULL, NULL, NULL, NULL),
("7FuXxDju2e", NULL, NULL, NULL, 2020, "2021-11-12", NULL, TRUE, NULL, NULL, NULL, 11, NULL, NULL, NULL),
("vhRvEQSU9f", NULL, NULL, NULL, 2020, "2020-04-30", "windows", TRUE, NULL, NULL, NULL, NULL, 46, NULL, NULL),
("TW5RvYLqJk", NULL, NULL, NULL, 2015, "2017-02-12", NULL, TRUE, NULL, NULL, NULL, NULL, NULL, 68, NULL),
("M6DwduZFbF", NULL, NULL, NULL, 2012, "2015-01-01", NULL, TRUE, NULL, NULL, NULL, NULL, NULL, NULL, 8),
("qPLdBqGQur", NULL, NULL, NULL, 2019, "2019-03-22", "macOS", TRUE, NULL, NULL, NULL, NULL, 48, NULL, NULL),
("LvbgrDm7ri", NULL, NULL, NULL, 2021, "2021-01-02", "windows", FALSE, NULL, NULL, NULL, NULL, 42, NULL, NULL),
("nFBty8g9sx", NULL, NULL, NULL, 2021, "2021-10-30", "linux", TRUE, NULL, NULL, NULL, NULL, 47, NULL, NULL);
INSERT INTO room
VALUES 
(1, "bad room", 1),
(2, "good room", 2),
(3, "medium room", 3),
(4, "smelly room", 4),
(5, "stinky room", 5),
(6, "computer room", 6),
(7, "bad room", 7),
(8, "good room", 8),
(9, "computer room", 9),
(10, "server room", 10);
INSERT INTO rack
VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3),
(4, 4, 4),
(5, 5, 5),
(6, 6, 6),
(7, 7, 7),
(8, 8, 8),
(9, 9, 9),
(10, 10, 10);
INSERT INTO owner_of
VALUES
("qPLdBqGQur", 1, 1, 1, 1),
("LvbgrDm7ri", 2, 2, 2, 2),
("TW5RvYLqJk", 1, 1, 1, 1),
("Au8TzFWq", 4, 4, 4, 4),
("aRtN6L7p", 5, 5, 5, 5),
("YBh2FkWB", 6, 6, 6, 6),
("M6DwduZFbF", 7, 7, 7, 7),
("MJo8hSHG", 8, 8, 8, 8),
("beBqveQj", 9, 9, 9, 9),
("ykSuAjzA", 10, 10, 10, 10);
