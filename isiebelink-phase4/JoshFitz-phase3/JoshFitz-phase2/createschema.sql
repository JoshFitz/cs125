CREATE TABLE manufacturer
(
  manuf_id INT PRIMARY KEY NOT NULL,
  name VARCHAR(256),
  support_site TEXT
);

CREATE TABLE building
(
  bldg_id INT PRIMARY KEY NOT NULL,
  campus VARCHAR(256),
  name VARCHAR(256),
  capacity INT
);

CREATE TABLE person
(
  person_id INT PRIMARY KEY NOT NULL,
  fname VARCHAR(256),
  lname VARCHAR(256),
  phone VARCHAR(256) CHECK (phone LIKE '_____' or phone LIKE '____________'),
  email VARCHAR(256),
  role VARCHAR(256)
 /* blding_id INT */
);

CREATE TABLE department
(
  dept_id INT PRIMARY KEY NOT NULL,
  name VARCHAR(256),
  head INT NOT NULL,
  division VARCHAR(256)
);

CREATE TABLE server_model
(
  model_id INT PRIMARY KEY NOT NULL,
  cpu VARCHAR(256),
  ram VARCHAR(256),
  os VARCHAR(256),
  hd_size VARCHAR(256),
  virtual BOOLEAN,
  manuf_id INT NOT NULL,
  FOREIGN KEY (model_id) REFERENCES device(server_id)
);

CREATE TABLE works_in
(
  person_id INT PRIMARY KEY NOT NULL,
  bldg_id INT PRIMARY KEY NOT NULL
);

CREATE TABLE member_of
(
  person_id INT PRIMARY KEY NOT NULL,
  dept_id INT PRIMARY KEY NOT NULL
);

CREATE TABLE camera_model
(
  recording BOOLEAN,
  range_ft INT,
  resolution VARCHAR(256),
  field_of_view INT,
  model_id INT PRIMARY KEY NOT NULL,
  manuf_id INT NOT NULL,
  FOREIGN KEY (model_id) REFERENCES device(camera_id)
);

CREATE TABLE projector_model
(
  model_id INT PRIMARY KEY NOT NULL,
  hdmi INT,
  wi_fi BOOLEAN,
  lumens INT,
  speakers BOOLEAN,
  resolution VARCHAR(256),
  manuf_id INT NOT NULL,
  FOREIGN KEY (model_id) REFERENCES device(projector_id)
);

CREATE TABLE printer_model
(
  model_id INT PRIMARY KEY NOT NULL,
  double_sided BOOLEAN,
  paper_size VARCHAR(256),
  memory INT,
  speed INT,
  laser BOOLEAN,
  bw_color VARCHAR(256),
  manuf_id INT NOT NULL,
  FOREIGN KEY (model_id) REFERENCES device(printer_id)
);

CREATE TABLE computer_model
(
  model_id INT PRIMARY KEY NOT NULL,
  cpu VARCHAR(256),
  screen_size INT,
  resolution VARCHAR(256),
  ram INT,
  os VARCHAR(256), 
  type VARCHAR(256),
  hd_size INT,
  manuf_id INT NOT NULL,
  FOREIGN KEY (model_id) REFERENCES device(computer_id),
  CHECK (os LIKE '%linux%' OR os LIKE '%mac%' OR os LIKE '%windows%' OR os LIKE '%Linux%')
);

CREATE TABLE monitor_model
(
  model_id INT PRIMARY KEY NOT NULL,
  hdmi INT CHECK (hdmi <= 5),
  panel_type VARCHAR(256),
  resolution VARCHAR(256),
  size INT,
  refresh_rate INT,
  manuf_id INT NOT NULL,
  FOREIGN KEY (model_id) REFERENCES device(monitor_id)
);

CREATE TABLE nwdevice_model
(
  model_id INT PRIMARY KEY NOT NULL,
  mac_address VARCHAR(256),
  protocol VARCHAR(256),
  type VARCHAR(256),
  speed INT,
  number_ports INT,
  coverage INT,
  number_clients INT,
  manuf_id INT NOT NULL,
  FOREIGN KEY (model_id) REFERENCES device(nwdevice_id)
);

CREATE TABLE device
(
  serial_number PRIMARY KEY VARCHAR(256),
  custom_cpu VARCHAR(256),
  custom_ram VARCHAR(256),
  custom_hdsize INT,
  year_manufac INT,
  date_purchased DATE,
  os VARCHAR(256),
  active BOOLEAN,
  monitor_id INT REFERENCES monitor_model(model_id),
  printer_id INT REFERENCES printer_model(model_id),
  projector_id INT REFERENCES projector_model(model_id),
  camera_id INT REFERENCES camera_model(model_id),
  computer_id INT REFERENCES computer_model(model_id),
  nwdevice_id INT REFERENCES nwdevice_model(model_id),
  server_id INT REFERENCES server_model(model_id),
  FOREIGN KEY (serial_number) REFERENCES owner_of(serial_number)
);

CREATE TABLE room
(
  room_id INT PRIMARY KEY NOT NULL,
  description TEXT,
  bldg_id INT NOT NULL
);

CREATE TABLE rack
(
  slot INT NOT NULL,
  rack_id INT PRIMARY KEY NOT NULL,
  room_id INT NOT NULL
);

CREATE TABLE owner_of
(
  serial_number VARCHAR(256) NOT NULL,
  person_id INT REFERENCES person(person_id),
  dept_id INT REFERENCES department(dept_id),
  room_id INT REFERENCES room(room_id),
  rack_id INT REFERENCES rack(rack_id)
);
