/* Goal of Data Modifaction Statement: Change all the devices that have a manufactured date before 2015 to inactive */
/* Statement 1 */
UPDATE device
SET active = false
WHERE year_manufac < 2015;


/* Goal of Data Modifaction Statement: Change all devices that are not currently assigned to anyone to inactive */
/* Statement 2 */ 
UPDATE device 
SET active = false
WHERE serial_number NOT IN(SELECT serial_number
                            FROM owner_of);


/* Goal of Data Modifaction Statement: Reassign the routers currently in VK to Clark */
/* Statement 3 */
UPDATE owner_of
SET owner_of.room_id = 6
WHERE owner_of.serial_number IN(SELECT owner_of.serial_number
                                FROM nwdevice_model, device, owner_of, rack, room
                                WHERE nwdevice_model.type = "Router"
                                AND device.nwdevice_id = nwdevice_model.model_id
                                AND device.serial_number = owner_of.serial_number
                                AND owner_of.room_id = room.room_id
                                AND room.bldg_id = 1);


/* Goal of Data Modifaction Statement: Deleting all linux computer devices that were made 10 or more years ago */
/* Statement 4 */
DELETE FROM device
WHERE device.serial_number IN(SELECT device.serial_number 
                            FROM computer_model, device
                            WHERE computer_model.model_id = device.computer_id
                            AND computer_model.os = "linux"
                            AND year_manufac < 2011);


/* Goal of Data Modifaction Statement: Deleting all monitors with 1 or 0 hdmi ports */
/* Statement 5 */
DELETE FROM monitor_model
WHERE hdmi <= 1;