DROP TABLE IF EXISTS manufacturer CASCADE;
DROP TABLE IF EXISTS building CASCADE;
DROP TABLE IF EXISTS server_model CASCADE;
DROP TABLE IF EXISTS camera_model CASCADE;
DROP TABLE IF EXISTS projector_model CASCADE;
DROP TABLE IF EXISTS printer_model CASCADE;
DROP TABLE IF EXISTS computer_model CASCADE;
DROP TABLE IF EXISTS monitor_model CASCADE;
DROP TABLE IF EXISTS nwdevice_model CASCADE;
DROP TABLE IF EXISTS device CASCADE;
DROP TABLE IF EXISTS person CASCADE;
DROP TABLE IF EXISTS room CASCADE;
DROP TABLE IF EXISTS department CASCADE;
DROP TABLE IF EXISTS rack CASCADE;
DROP VIEW IF EXISTS oldest_devices CASCADE;
DROP VIEW IF EXISTS personal_computers CASCADE;