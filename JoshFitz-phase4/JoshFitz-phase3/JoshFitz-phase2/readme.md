# Phase 2 - Josh Fitzgerald. Consulted/Worked with Landon Moir and Ben Arenchild


Files in JoshFitz-phase2:

1. src (folder)
    - createLargeDump.py
        - This is the python script written primarily by Landon Moir that populates "populatelarge.sql" with the appropriate number of tuples with their corresponding types. This was made by importing the faker library.

2. createschema.sql
    - This file is a modified version of the skeleton the class received from Professor Rodkey. We took out all the key declarations and changed the types to match realistic data inputs.

3. dropschema.sql
    - This file contains all the drop table commands.

4. populatesmall.sql
    - Made primarily by Ben Arenchild and Josh Fitzgerald, this file contains approximately 10 tuples for each relation, with realstic yet fictitious data put into each relation using the INSERT command.

5. populatelarge.sql
    - Made primarily by Landon Moir through the createLargeDump.py python script, this file had multiple relations with tens of thousands of tuples, several with thousands, and still several with hundreds. The contents of these tuples are not necessarily realistic yet still accomodates interactive queries.

6. JoshFitzERdiagram.erdplus
    - This is the updated ER Diagram decided on by the class that our database was modeled after.

