# Phase 3 - Josh Fitzgerald


Files in JoshFitz-phase3:

1. JoshFitz-phase2
    - Folder that contains the contents of the last assignment, with some updates, per project specifications

2. a.sql
    - This file contains 5 different queries that find different information within the database and includes different types of operations that SQL offers

3. b.sql
    - This file contains 5 different data modifications statements, inluding 3 statements with subqueries

4. c.sql
    - This file contains 2 different views and corresponding queries based on those views

5. d.sql
    - This file contains 2 different indexes for aspects of the database that will be searched for extensively, along with explained queries using those indexes and corresponding drop statements

6. readme.md
    - This file summarizes the purpose of each file
