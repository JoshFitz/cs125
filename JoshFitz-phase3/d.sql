/* INDEX 1 */
CREATE INDEX active_devices
ON device(serial_number, computer_id, projector_id, nwdevice_id, monitor_id, printer_id, camera_id, server_id, year_manufac, active);

EXPLAIN SELECT AVG(computer_model.ram)
FROM computer_model JOIN device ON computer_model.model_id = device.computer_id
WHERE device.active;

DROP INDEX active_devices;


/* INDEX 2 */
CREATE INDEX manufac_names 
ON manufacturer(name);

EXPLAIN SELECT COUNT(*)
FROM manufacturer, computer_model
WHERE manufacturer.manuf_id = computer_model.manuf_id
AND manufacturer.name = "microsoft";

DROP INDEX manufac_names;