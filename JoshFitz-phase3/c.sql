/* VIEW 1: Oldest devices currently in IT */
CREATE View oldest_devices AS 
SELECT year_manufac, serial_number, active
FROM device
WHERE year_manufac < (SELECT AVG(year_manufac) FROM device)
ORDER BY year_manufac
LIMIT 100;

/* Query for VIEW 1: What devices are currently being used that are older than the average age of Westmont's devices */
SELECT year_manufac, serial_number
FROM oldest_devices
WHERE active ;



/* VIEW 2: All computers owned by Westmont IT assigned to people */
CREATE View personal_computers AS 
SELECT device.computer_id, device.serial_number, computer_model.type, manufacturer.name, person.*
FROM computer_model, device, owner_of, person, manufacturer
WHERE manufacturer.manuf_id = computer_model.manuf_id
AND computer_model.model_id = device.computer_id
AND device.serial_number = owner_of.serial_number
AND owner_of.person_id = person.person_id
LIMIT 100;

/* Query for VIEW 2: Getting all the personal computers made by microsoft */
SELECT *
FROM personal_computers
WHERE name = "microsoft";