# Author: Landon Moir
from faker import Faker
from faker_schema.faker_schema import FakerSchema
from faker_web import WebProvider
import re

fake = Faker()
fake.add_provider(WebProvider)

faker = FakerSchema(faker=fake)


# ! Schemas
MANUFACTURER = {
                "manuf_id" : "random_int",
                "name" : "company",
                "support_site" : "street_address"
                }
BUILDING = {
            "bldg_id": "random_int",
            "campus": "word",
            "name": "color_name",
            "capacity": "random_int"
            }
PERSON = {
            "person_id": "random_int",
            "fname": "first_name",
            "lname": "last_name",
            "phone": "postcode",
            "email": "ascii_email",
            "role": "suffix"
            }
DEPARTMENT = {
              "dept_id": "random_int",
              "name": "company",
              "head": "random_int",
              "division": "company_suffix"
             }
SERVER_MODEL = {
                "model_id": "random_int",
                "cpu": "linux_processor",
                "ram": 'mac_processor',
                "os": "mac_platform_token",
                "hd_size": "random_int",
                "virtual": "pybool",
                "manuf_id": "random_int"
               }
WORKS_IN = {
            "person_id": "random_int",
            "bldg_id": "random_int"
           }
MEMBER_OF = {
             "person_id": "random_int",
             "dept_id": "random_int",
            }
CAMERA_MODEL = {
                "recording": "pybool",
                "range": "random_int",
                "resolution": "random_int",
                "field_of_view": "random_int",
                "model_id": "random_int",
                "manuf_id": "random_int"
               }
PROJECTOR_MODEL = {
                   "model_id": "random_int",
                   "hdmi": "random_int",
                   "wi-fi": "pybool",
                   "lumens": "random_int",
                   "speakers": "pybool",
                   "resolution": "random_int",
                   "manuf_id": "random_int"
                  }
PRINTER_MODEL = {
                 "model_id": "random_int",
                 "double_sided": "pybool",
                 "paper_size": "mac_processor",
                 "memory": "random_int",
                 "speed": "random_int",
                 "laser": "pybool",
                 "bw_color": "hex_color",
                 "manuf_id": "random_int"
                }
COMPUTER_MODEL = {
                  "model_id": "random_int",
                  "cpu": "linux_processor",
                  "screen_size": "random_int",
                  "resolution": "random_int",
                  "ram": "random_int",
                  "os": "linux_platform_token",
                  "type": "android_platform_token",
                  "hd_size": "random_int",
                  "manuf_id": "random_int"
                 }
MONITOR_MODEL = {
                 "model_id": "random_int",
                 "hdmi": "random_int",
                 "panel_type": "file_extension",
                 "resolution": "random_int",
                 "size": "random_int",
                 "refresh_rate": "random_int",
                 "manuf_id": "random_int"
                }
NWDEVICE_MODEL = {
                  "model_id": "random_int",
                  "mac_address": "mac_address",
                  "protocol": "ipv4_network_class",
                  "type": "tld",
                  "speed": "random_int",
                  "number_ports": "random_int",
                  "coverage": "random_int",
                  "number_clients": "random_int",
                  "manuf_id": "random_int"
                 }
DEVICE = {
          "serial_number": "md5",
          "custom_cpu": "windows_platform_token",
          "custom_ram": "random_int",
          "custom_hdsize": "random_int",
          "year_manufac": "random_int",
          "date_purchased": "date",
          "os": "windows_platform_token",
          "active": "pybool",
          "monitor_id": "random_int",
          "printer_id": "random_int",
          "projector_id": "random_int",
          "camera_id": "random_int",
          "computer_id": "random_int",
          "nwdevice_id": "random_int",
          "server_id": "random_int"
         }
ROOM = {
        "room_id": "random_int",
        "description": "paragraph",
        "bldg_id": "random_int"
       }
RACK = {
        "slot": "random_int",
        "rack_id": "random_int",
        "room_id": "random_int"
        }
OWNER = {
         "serial_number": "md5",
         "person_id": "random_int",
         "dept_id": "random_int",
         "room_id": "random_int",
         "rack_id": "random_int"
        }

SCHEMA_LIST = {'manufacturer': [MANUFACTURER, 500],
               'building': [BUILDING, 250],
               'person': [PERSON, 10000],
               'department': [DEPARTMENT, 300],
               'server_model': [SERVER_MODEL, 1000],
               'works_in': [WORKS_IN, 1000],
               'member_of': [MEMBER_OF, 2500],
               'camera_model': [CAMERA_MODEL, 5000],
               'projector_model': [PROJECTOR_MODEL, 2500],
               'printer_model': [PRINTER_MODEL, 450],
               'computer_model': [COMPUTER_MODEL, 10000],
               'monitor_model': [MONITOR_MODEL, 3000],
               'nwdevice_model': [NWDEVICE_MODEL, 1000],
               'device': [DEVICE, 10000],
               'room': [ROOM, 1231],
               'rack': [RACK, 1232],
               'owner_of': [OWNER, 420]
}

def addToDump(schema, name, amount, dumpFile):
    dumpFile.write(f"INSERT INTO {name} VALUES\n")
    for i in range(amount):
        fd = faker.generate_fake(schema)
        dumpFile.write(buildTuple(fd))
    dumpFile.write(';\n')

def buildTuple(fd):
    string = "("
    for key in fd:
        if isinstance(fd[key], str):
            string += f'"{fd[key]}", '
        else:
            string += f'{fd[key]}, '
    string += "),\n"
    string = string.replace(", )", ")")
    return string

if __name__ == "__main__":
    with open('JoshFitz-phase2/populatelarge.sql', "w") as outfile:
            for key in SCHEMA_LIST:
                addToDump(SCHEMA_LIST[key][0], key, SCHEMA_LIST[key][1], outfile)
    outfile.close()
    with open('JoshFitz-phase2/populatelarge.sql', 'r+') as infile:
        text = infile.read()
        text = re.sub(r'(,\n;)+', ';', text)
        infile.seek(0)
        infile.write(text)
        infile.truncate()