CREATE TABLE manufacturer
(
  manuf_id INT NOT NULL,
  name VARCHAR(256),
  support_site TEXT
);

CREATE TABLE building
(
  bldg_id INT NOT NULL,
  campus VARCHAR(256),
  name VARCHAR(256),
  capacity INT
);

CREATE TABLE person
(
  person_id INT NOT NULL,
  fname VARCHAR(256),
  lname VARCHAR(256),
  phone VARCHAR(256),
  email VARCHAR(256),
  role VARCHAR(256)
);

CREATE TABLE department
(
  dept_id INT NOT NULL,
  name VARCHAR(256),
  head INT NOT NULL,
  division VARCHAR(256)
);

CREATE TABLE server_model
(
  model_id INT NOT NULL,
  cpu VARCHAR(256),
  ram VARCHAR(256),
  os VARCHAR(256),
  hd_size VARCHAR(256),
  virtual BOOLEAN,
  manuf_id INT NOT NULL
);

CREATE TABLE works_in
(
  person_id INT NOT NULL,
  bldg_id INT NOT NULL
);

CREATE TABLE member_of
(
  person_id INT NOT NULL,
  dept_id INT NOT NULL
);

CREATE TABLE camera_model
(
  recording BOOLEAN,
  range_ft INT,
  resolution VARCHAR(256),
  field_of_view INT,
  model_id INT NOT NULL,
  manuf_id INT NOT NULL
);

CREATE TABLE projector_model
(
  model_id INT NOT NULL,
  hdmi INT,
  wi_fi BOOLEAN,
  lumens INT,
  speakers BOOLEAN,
  resolution VARCHAR(256),
  manuf_id INT NOT NULL
);

CREATE TABLE printer_model
(
  model_id INT NOT NULL,
  double_sided BOOLEAN,
  paper_size VARCHAR(256),
  memory INT,
  speed INT,
  laser BOOLEAN,
  bw_color VARCHAR(256),
  manuf_id INT NOT NULL
);

CREATE TABLE computer_model
(
  model_id INT NOT NULL,
  cpu VARCHAR(256),
  screen_size INT,
  resolution VARCHAR(256),
  ram INT,
  os VARCHAR(256), 
  type VARCHAR(256),
  hd_size INT,
  manuf_id INT NOT NULL
);

CREATE TABLE monitor_model
(
  model_id INT NOT NULL,
  hdmi INT,
  panel_type VARCHAR(256),
  resolution VARCHAR(256),
  size INT,
  refresh_rate INT,
  manuf_id INT NOT NULL
);

CREATE TABLE nwdevice_model
(
  model_id INT NOT NULL,
  mac_address VARCHAR(256),
  protocol VARCHAR(256),
  type VARCHAR(256),
  speed INT,
  number_ports INT,
  coverage INT,
  number_clients INT,
  manuf_id INT NOT NULL
);

CREATE TABLE device
(
  serial_number VARCHAR(256),
  custom_cpu VARCHAR(256),
  custom_ram VARCHAR(256),
  custom_hdsize INT,
  year_manufac INT,
  date_purchased DATE,
  os VARCHAR(256),
  active BOOLEAN,
  monitor_id INT,
  printer_id INT,
  projector_id INT,
  camera_id INT,
  computer_id INT,
  nwdevice_id INT,
  server_id INT
);

CREATE TABLE room
(
  room_id INT NOT NULL,
  description TEXT,
  bldg_id INT NOT NULL
);

CREATE TABLE rack
(
  slot INT NOT NULL,
  rack_id INT NOT NULL,
  room_id INT NOT NULL
);

CREATE TABLE owner_of
(
  serial_number VARCHAR(256) NOT NULL,
  person_id INT,
  dept_id INT,
  room_id INT,
  rack_id INT
);
