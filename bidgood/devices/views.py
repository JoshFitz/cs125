from django.shortcuts import render
from django.views import generic
from django.views.generic.list import ListView
from devices.models import *
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

from .models import *

def index(request):
    return render(request, "devices/index.html" #, context = data)
    )


def viewAllDevices(request):
    return render(request, "devices/viewAllDevicesRefactor.html")


class ComputerListView(ListView):
    model = Computer_Model

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['computer_list'] = Computer_Model.objects.all()
        return context

class ComputerDetailView(generic.DetailView):
    model = Computer_Model

class ComputerCreate(CreateView):
    model = Computer_Model
    fields = "__all__"

class ComputerUpdate(UpdateView):
    model = Computer_Model
    fields = '__all__' 

class ComputerDelete(DeleteView):
    model = Computer_Model
    success_url = reverse_lazy('computer_model_list')


class MonitorListView(ListView):
    model = Monitor_Model   

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['monitor_list'] = Monitor_Model.objects.all()
        return context

class MonitorDetailView(generic.DetailView):
    model = Monitor_Model

class MonitorCreate(CreateView):
    model = Monitor_Model
    fields = "__all__"

class MonitorUpdate(UpdateView):
    model = Monitor_Model
    fields = '__all__' 

class MonitorDelete(DeleteView):
    model = Monitor_Model
    success_url = reverse_lazy('monitor_model_list')


class PrinterListView(ListView):
    model = Printer_Model

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['printer_list'] = Printer_Model.objects.all()
        return context

class PrinterDetailView(generic.DetailView):
    model = Printer_Model

class PrinterCreate(CreateView):
    model = Printer_Model
    fields = "__all__"

class PrinterUpdate(UpdateView):
    model = Printer_Model
    fields = '__all__' 

class PrinterDelete(DeleteView):
    model = Printer_Model
    success_url = reverse_lazy('printer_model_list')