from django import forms
"""
class AddDevice(forms.Form):
    
    serial_number = forms.CharField(max_length=256, primary_key=True, unique=True)
    manuf = forms.ForeignKey(Manufacturer, on_delete=models.CASCADE, blank=True, null=True)
    custom_cpu = forms.CharField(max_length=256, blank=True, null=True)
    custom_ram = models.CharField(max_length=256, blank=True, null=True)
    custom_hdsize = models.IntegerField(blank=True, null=True)
    year_manufac = models.IntegerField()
    date_purchased = forms.DateField(help_text="Enter the date the device was purchased.")
    os = models.CharField(max_length=256, blank=True, null=True)
    #This active field needs to be updated to accomodate different life cycles... maybe just make a new table?
    stage = models.ForeignKey(Lifecycle, on_delete=models.CASCADE, blank=True, null=True)
    monitor = models.ForeignKey(Monitor_Model, on_delete=models.CASCADE, blank=True, null=True)
    printer = models.ForeignKey(Printer_Model, on_delete=models.CASCADE, blank=True, null=True)
    projector = models.ForeignKey(Projector_Model, on_delete=models.CASCADE, blank=True, null=True)
    camera = models.ForeignKey(Camera_Model, on_delete=models.CASCADE, blank=True, null=True)
    computer = models.ForeignKey(Computer_Model, on_delete=models.CASCADE, blank=True, null=True)
    nwdevice = models.ForeignKey(Nwdevice_Model, on_delete=models.CASCADE, blank=True, null=True)
    server = models.ForeignKey(Server_Model, on_delete=models.CASCADE, blank=True, null=True)
    rack = models.ForeignKey(Rack, on_delete=models.CASCADE, blank=True, null=True)
    dept = models.ForeignKey(Department, on_delete=models.CASCADE, blank=True, null=True)
    room = models.ForeignKey(Room, on_delete=models.CASCADE, blank=True, null=True)
    person = models.ForeignKey(Person, on_delete=models.CASCADE, blank=True, null=True)

"""