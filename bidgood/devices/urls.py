from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('viewAllDevices/', views.viewAllDevices, name='viewAllDevices'),
    path('computers/', views.ComputerListView.as_view(), name='computer_model_list'),
    path('computer/<int:pk>', views.ComputerDetailView.as_view(), name='computer_model_detail'),
    path('computer/create/', views.ComputerCreate.as_view(), name='computer_create'),
    path('computer/<int:pk>/update/', views.ComputerUpdate.as_view(), name='computer_update'),
    path('computer/<int:pk>/delete/', views.ComputerDelete.as_view(), name='computer_delete'),
    path('monitors/', views.MonitorListView.as_view(), name='monitor_model_list'),
    path('monitor/<int:pk>', views.MonitorDetailView.as_view(), name='monitor_model_detail'),
    path('monitor/create/', views.MonitorCreate.as_view(), name='monitor_create'),
    path('monitor/<int:pk>/update/', views.MonitorUpdate.as_view(), name='monitor_update'),
    path('monitor/<int:pk>/delete/', views.MonitorDelete.as_view(), name='monitor_delete'),
    path('printers/', views.PrinterListView.as_view(), name='printer_model_list'),
    path('printer/<int:pk>', views.PrinterDetailView.as_view(), name='printer_model_detail'),
    path('printer/create/', views.PrinterCreate.as_view(), name='printer_create'),
    path('printer/<int:pk>/update/', views.PrinterUpdate.as_view(), name='printer_update'),
    path('printer/<int:pk>/delete/', views.PrinterDelete.as_view(), name='printer_delete'),
]   