from django.contrib import admin

#from .models import Device
from .models import *

#admin.site.register(Device)
admin.site.register(Server_Model)
admin.site.register(Camera_Model)
admin.site.register(Monitor_Model)
admin.site.register(Printer_Model)
admin.site.register(Projector_Model)
admin.site.register(Computer_Model)
admin.site.register(Nwdevice_Model)
admin.site.register(Rack)
admin.site.register(Person)
admin.site.register(Building)
admin.site.register(Room)
admin.site.register(Department)
admin.site.register(Manufacturer)
admin.site.register(Lifecycle)