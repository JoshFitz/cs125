from enum import unique
from django.db import models
from django.urls import reverse
from django.forms import ModelForm

# Create your models here.
class Manufacturer(models.Model):
    manuf_id = models.AutoField(primary_key=True, unique=True)
    name = models.CharField(max_length=256)  
    support_site = models.CharField(max_length=256)

    def __str__(self):
        return (f"{self.name}")


class Building(models.Model):
    building_id = models.AutoField(primary_key=True, unique=True)
    campus = models.CharField(max_length=256)  
    name = models.CharField(max_length=256)
    capacity = models.IntegerField()

    def __str__(self):
        return (f"{self.name}")


class Person(models.Model):
    person_id = models.AutoField(primary_key=True, unique=True)
    fname = models.CharField(max_length=256)  
    lname = models.CharField(max_length=256)
    phone = models.CharField(max_length=256)
    email = models.CharField(max_length=256)
    building = models.ForeignKey(Building, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return (f"{self.fname} {self.lname}")


class Department(models.Model):
    department_id = models.AutoField(primary_key=True, unique=True)
    name = models.CharField(max_length=256, unique=True)
    head = models.ForeignKey(Person, on_delete=models.CASCADE, blank=True, null=True)
    division = models.CharField(max_length=256)

    def __str__(self):
        return (f"{self.name}")


class Room(models.Model):
    room_id = models.AutoField(primary_key=True, unique=True)
    name = models.CharField(max_length=256)
    building = models.ForeignKey(Building, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return (f"{self.name}, \n" +
                f"{self.building}")
                

class Rack(models.Model):
    rack_id = models.AutoField(primary_key=True, unique=True)
    slot = models.IntegerField()
    room = models.ForeignKey(Room, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return (f"Rack ID: {self.rack_id}, \n" +
                f"Slot: {self.slot}, \n" +
                f"Room ID: {self.room}")


class Lifecycle(models.Model):
    stage = models.CharField(primary_key=True, max_length=256, unique=True)

    def __str__(self):
        return (f"{self.stage}")


class Server_Model(models.Model):
    model_id = models.AutoField(primary_key=True)
    cpu = models.CharField(max_length=256)
    ram = models.CharField(max_length=256)
    os = models.CharField(max_length=256)
    hd_size = models.CharField(max_length=256)
    virtual = models.BooleanField()  
    serial_number = models.CharField(max_length=256, unique=True, blank=True, null=True)
    manuf = models.ForeignKey(Manufacturer, on_delete=models.CASCADE, blank=True, null=True)
    custom_cpu = models.CharField(max_length=256, blank=True, null=True)
    custom_ram = models.CharField(max_length=256, blank=True, null=True)
    custom_hdsize = models.IntegerField(blank=True, null=True)
    year_manufac = models.IntegerField(blank=True, null=True)
    date_purchased = models.DateTimeField(blank=True, null=True)
    os = models.CharField(max_length=256, blank=True, null=True)
    stage = models.ForeignKey(Lifecycle, on_delete=models.CASCADE, blank=True, null=True)
    rack = models.ForeignKey(Rack, on_delete=models.CASCADE, blank=True, null=True)
    dept = models.ForeignKey(Department, on_delete=models.CASCADE, blank=True, null=True)
    room = models.ForeignKey(Room, on_delete=models.CASCADE, blank=True, null=True)
    person = models.ForeignKey(Person, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self): 
        return (f"{self.model_id}")


class Camera_Model(models.Model):
    model_id = models.AutoField(primary_key=True)
    recording = models.BooleanField()
    range_ft = models.IntegerField()
    resolution = models.CharField(max_length=256)
    field_of_view = models.IntegerField() 
    serial_number = models.CharField(max_length=256, unique=True, blank=True, null=True)
    manuf = models.ForeignKey(Manufacturer, on_delete=models.CASCADE, blank=True, null=True)
    custom_cpu = models.CharField(max_length=256, blank=True, null=True)
    custom_ram = models.CharField(max_length=256, blank=True, null=True)
    custom_hdsize = models.IntegerField(blank=True, null=True)
    year_manufac = models.IntegerField(blank=True, null=True)
    date_purchased = models.DateTimeField(blank=True, null=True)
    os = models.CharField(max_length=256, blank=True, null=True)
    stage = models.ForeignKey(Lifecycle, on_delete=models.CASCADE, blank=True, null=True)
    rack = models.ForeignKey(Rack, on_delete=models.CASCADE, blank=True, null=True)
    dept = models.ForeignKey(Department, on_delete=models.CASCADE, blank=True, null=True)
    room = models.ForeignKey(Room, on_delete=models.CASCADE, blank=True, null=True)
    person = models.ForeignKey(Person, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return (f"{self.model_id}")


class Projector_Model(models.Model):
    model_id = models.AutoField(primary_key=True)
    hdmi = models.IntegerField()
    wi_fi = models.BooleanField()
    lumens = models.IntegerField()
    speakers = models.BooleanField()
    resolution = models.CharField(max_length=256)
    serial_number = models.CharField(max_length=256, unique=True, blank=True, null=True)
    manuf = models.ForeignKey(Manufacturer, on_delete=models.CASCADE, blank=True, null=True)
    custom_cpu = models.CharField(max_length=256, blank=True, null=True)
    custom_ram = models.CharField(max_length=256, blank=True, null=True)
    custom_hdsize = models.IntegerField(blank=True, null=True)
    year_manufac = models.IntegerField(blank=True, null=True)
    date_purchased = models.DateTimeField(blank=True, null=True)
    os = models.CharField(max_length=256, blank=True, null=True)
    stage = models.ForeignKey(Lifecycle, on_delete=models.CASCADE, blank=True, null=True)
    rack = models.ForeignKey(Rack, on_delete=models.CASCADE, blank=True, null=True)
    dept = models.ForeignKey(Department, on_delete=models.CASCADE, blank=True, null=True)
    room = models.ForeignKey(Room, on_delete=models.CASCADE, blank=True, null=True)
    person = models.ForeignKey(Person, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return (f"{self.model_id}")


class Printer_Model(models.Model):
    model_id = models.AutoField(primary_key=True)
    double_sided = models.BooleanField()
    paper_size = models.CharField(max_length=256)
    memory = models.IntegerField()
    speed = models.IntegerField()
    bw_color = models.CharField(max_length=256)
    serial_number = models.CharField(max_length=256, unique=True, blank=True, null=True)
    manuf = models.ForeignKey(Manufacturer, on_delete=models.CASCADE, blank=True, null=True)
    custom_cpu = models.CharField(max_length=256, blank=True, null=True)
    custom_ram = models.CharField(max_length=256, blank=True, null=True)
    custom_hdsize = models.IntegerField(blank=True, null=True)
    year_manufac = models.IntegerField(blank=True, null=True)
    date_purchased = models.DateTimeField(blank=True, null=True)
    os = models.CharField(max_length=256, blank=True, null=True)
    stage = models.ForeignKey(Lifecycle, on_delete=models.CASCADE, blank=True, null=True)
    rack = models.ForeignKey(Rack, on_delete=models.CASCADE, blank=True, null=True)
    dept = models.ForeignKey(Department, on_delete=models.CASCADE, blank=True, null=True)
    room = models.ForeignKey(Room, on_delete=models.CASCADE, blank=True, null=True)
    person = models.ForeignKey(Person, on_delete=models.CASCADE, blank=True, null=True)

    def get_absolute_url(self):
        return reverse('printer_model_detail', args=[str(self.model_id)])

    def __str__(self):
        return (f"{self.model_id}")


class Computer_Model(models.Model):
    model_id = models.AutoField(primary_key=True)
    cpu = models.CharField(max_length=256)
    screen_size = models.IntegerField(blank=True, null=True)
    resolution = models.CharField(max_length=256, blank=True, null=True)
    ram = models.IntegerField()
    os = models.CharField(max_length=256)
    type = models.CharField(max_length=256)
    hd_size = models.IntegerField()
    serial_number = models.CharField(max_length=256, unique=True, blank=True, null=True)
    manuf = models.ForeignKey(Manufacturer, on_delete=models.CASCADE, blank=True, null=True)
    custom_cpu = models.CharField(max_length=256, blank=True, null=True)
    custom_ram = models.CharField(max_length=256, blank=True, null=True)
    custom_hdsize = models.IntegerField(blank=True, null=True)
    year_manufac = models.IntegerField(blank=True, null=True)
    date_purchased = models.DateTimeField(blank=True, null=True)
    os = models.CharField(max_length=256, blank=True, null=True)
    stage = models.ForeignKey(Lifecycle, on_delete=models.CASCADE, blank=True, null=True)
    rack = models.ForeignKey(Rack, on_delete=models.CASCADE, blank=True, null=True)
    dept = models.ForeignKey(Department, on_delete=models.CASCADE, blank=True, null=True)
    room = models.ForeignKey(Room, on_delete=models.CASCADE, blank=True, null=True)
    person = models.ForeignKey(Person, on_delete=models.CASCADE, blank=True, null=True)

    def get_absolute_url(self):
        return reverse('computer_model_detail', args=[str(self.model_id)])

    def __str__(self):
        return (f"{self.model_id}")


class Monitor_Model(models.Model):
    model_id = models.AutoField(primary_key=True)
    panel_type = models.CharField(max_length=256)
    resolution = models.CharField(max_length=256)
    size = models.IntegerField()
    refresh_rate = models.IntegerField()
    serial_number = models.CharField(max_length=256, unique=True, blank=True, null=True)
    manuf = models.ForeignKey(Manufacturer, on_delete=models.CASCADE, blank=True, null=True)
    custom_cpu = models.CharField(max_length=256, blank=True, null=True)
    custom_ram = models.CharField(max_length=256, blank=True, null=True)
    custom_hdsize = models.IntegerField(blank=True, null=True)
    year_manufac = models.IntegerField(blank=True, null=True)
    date_purchased = models.DateTimeField(blank=True, null=True)
    os = models.CharField(max_length=256, blank=True, null=True)
    stage = models.ForeignKey(Lifecycle, on_delete=models.CASCADE, blank=True, null=True)
    rack = models.ForeignKey(Rack, on_delete=models.CASCADE, blank=True, null=True)
    dept = models.ForeignKey(Department, on_delete=models.CASCADE, blank=True, null=True)
    room = models.ForeignKey(Room, on_delete=models.CASCADE, blank=True, null=True)
    person = models.ForeignKey(Person, on_delete=models.CASCADE, blank=True, null=True)

    def get_absolute_url(self):
        return reverse('monitor_model_detail', args=[str(self.model_id)])

    def __str__(self):
        return (f"{self.model_id}")


class Nwdevice_Model(models.Model):
    model_id = models.AutoField(primary_key=True)
    mac_address = models.CharField(max_length=256)
    protocol = models.CharField(max_length=256)
    type = models.CharField(max_length=256)
    speed = models.IntegerField()
    number_ports = models.IntegerField()
    coverage = models.IntegerField()
    number_clients = models.IntegerField()
    serial_number = models.CharField(max_length=256, unique=True, blank=True, null=True)
    manuf = models.ForeignKey(Manufacturer, on_delete=models.CASCADE, blank=True, null=True)
    custom_cpu = models.CharField(max_length=256, blank=True, null=True)
    custom_ram = models.CharField(max_length=256, blank=True, null=True)
    custom_hdsize = models.IntegerField(blank=True, null=True)
    year_manufac = models.IntegerField(blank=True, null=True)
    date_purchased = models.DateTimeField(blank=True, null=True)
    os = models.CharField(max_length=256, blank=True, null=True)
    stage = models.ForeignKey(Lifecycle, on_delete=models.CASCADE, blank=True, null=True)
    rack = models.ForeignKey(Rack, on_delete=models.CASCADE, blank=True, null=True)
    dept = models.ForeignKey(Department, on_delete=models.CASCADE, blank=True, null=True)
    room = models.ForeignKey(Room, on_delete=models.CASCADE, blank=True, null=True)
    person = models.ForeignKey(Person, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return (f"{self.model_id}")


#Below is the original schema we were thinking... above it what i would look like assuming we get rid of the device relation
"""
class Server_Model(models.Model):
    model_id = models.AutoField(primary_key=True, unique=True)
    cpu = models.CharField(max_length=256)
    ram = models.CharField(max_length=256)
    os = models.CharField(max_length=256)
    hd_size = models.CharField(max_length=256)
    virtual = models.BooleanField()  

    def __str__(self): 
        return (f"Server ID: {self.model_id}")


class Camera_Model(models.Model):
    model_id = models.AutoField(primary_key=True, unique=True)
    recording = models.BooleanField()
    range_ft = models.IntegerField()
    resolution = models.CharField(max_length=256)
    field_of_view = models.IntegerField() 

    def __str__(self):
        return (f"Camera ID: {self.model_id}")


class Projector_Model(models.Model):
    model_id = models.AutoField(primary_key=True, unique=True) 
    hdmi = models.IntegerField()
    wi_fi = models.BooleanField()
    lumens = models.IntegerField()
    speakers = models.BooleanField()
    resolution = models.CharField(max_length=256)

    def __str__(self):
        return (f"Projector ID: {self.model_id}")


class Printer_Model(models.Model):
    model_id = models.AutoField(primary_key=True, unique=True) 
    double_sided = models.BooleanField()
    paper_size = models.CharField(max_length=256)
    memory = models.IntegerField()
    speed = models.IntegerField()
    bw_color = models.CharField(max_length=256)

    def __str__(self):
        return (f"Printer ID: {self.model_id}")


class Computer_Model(models.Model):
    model_id = models.AutoField(primary_key=True, unique=True)
    cpu = models.CharField(max_length=256)
    screen_size = models.IntegerField(blank=True, null=True)
    resolution = models.CharField(max_length=256, blank=True, null=True)
    ram = models.IntegerField()
    os = models.CharField(max_length=256)
    type = models.CharField(max_length=256)
    hd_size = models.IntegerField()

    def __str__(self):
        return (f"Computer ID: {self.model_id}")


class Monitor_Model(models.Model):
    model_id = models.AutoField(primary_key=True, unique=True)  
    panel_type = models.CharField(max_length=256)
    resolution = models.CharField(max_length=256)
    size = models.IntegerField()
    refresh_rate = models.IntegerField()

    def __str__(self):
        return (f"Monitor ID: {self.model_id}")


class Nwdevice_Model(models.Model):
    model_id = models.AutoField(primary_key=True, unique=True)  
    mac_address = models.CharField(max_length=256)
    protocol = models.CharField(max_length=256)
    type = models.CharField(max_length=256)
    speed = models.IntegerField()
    number_ports = models.IntegerField()
    coverage = models.IntegerField()
    number_clients = models.IntegerField()

    def __str__(self):
        return (f"Network Device ID: {self.model_id}")


class Device(models.Model):
    serial_number = models.CharField(max_length=256, primary_key=True, unique=True)
    manuf = models.ForeignKey(Manufacturer, on_delete=models.CASCADE, blank=True, null=True)
    custom_cpu = models.CharField(max_length=256, blank=True, null=True)
    custom_ram = models.CharField(max_length=256, blank=True, null=True)
    custom_hdsize = models.IntegerField(blank=True, null=True)
    year_manufac = models.IntegerField()
    date_purchased = models.DateTimeField()
    os = models.CharField(max_length=256, blank=True, null=True)
    #This active field needs to be updated to accomodate different life cycles... maybe just make a new table?
    stage = models.ForeignKey(Lifecycle, on_delete=models.CASCADE, blank=True, null=True)
    monitor = models.ForeignKey(Monitor_Model, on_delete=models.CASCADE, blank=True, null=True)
    printer = models.ForeignKey(Printer_Model, on_delete=models.CASCADE, blank=True, null=True)
    projector = models.ForeignKey(Projector_Model, on_delete=models.CASCADE, blank=True, null=True)
    camera = models.ForeignKey(Camera_Model, on_delete=models.CASCADE, blank=True, null=True)
    computer = models.ForeignKey(Computer_Model, on_delete=models.CASCADE, blank=True, null=True)
    nwdevice = models.ForeignKey(Nwdevice_Model, on_delete=models.CASCADE, blank=True, null=True)
    server = models.ForeignKey(Server_Model, on_delete=models.CASCADE, blank=True, null=True)
    rack = models.ForeignKey(Rack, on_delete=models.CASCADE, blank=True, null=True)
    dept = models.ForeignKey(Department, on_delete=models.CASCADE, blank=True, null=True)
    room = models.ForeignKey(Room, on_delete=models.CASCADE, blank=True, null=True)
    person = models.ForeignKey(Person, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return (f"Serial Number: {self.serial_number}, \n" + 
                f"Manufacturer: {self.manuf}, \n" +
                f"Status: {self.stage}")
"""