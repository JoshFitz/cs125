# Phase 5 - Josh Fitzgerald, Kevin Hodge, Isaac Sebelink, Frank Mugambage

Resources used/referenced to complete the work of this phase:
    - https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django
    - https://docs.djangoproject.com/en/3.2/intro/
    - https://stackoverflow.com/questions/60013978/django-class-based-views-not-rendering-variables-in-template
    - https://www.geeksforgeeks.org/listview-class-based-views-django/


Contents of Phase 5:
    - Bidgood
        - This is the folder that houses the entirety of our Django project. Features include:
            - Home Page that explains the project
            - View all devices page that lists all device models available in the database
            - List view of all instances of a current device model
            - Device detail page that lists all relevant attributes associated with any given device
            - Add, edit, and delete forms for all device models

    - isiebelink-phase4
        - contents of phase 4, with a readme.md that specifies the folders and files included

    - README.md 
        - file that lists the contents of the project

    - requirements.txt
        - all necessary extensions and downloads needed to run the django project

    - runtime.txt
        - version of python used for this project
